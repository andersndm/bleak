#version 330

in mat3 tbn;
in vec2 uv;
in vec3 pos;

layout(location = 0) out vec4 albedo_metallic;
layout(location = 1) out vec4 normal_roughness;
layout(location = 2) out vec4 frag_pos;

uniform sampler2D albedo_metallic_sampler;
uniform sampler2D normal_roughness_sampler;

uniform vec4 object_color;
uniform float emissive;

void main()
{
	vec4 albedo_metallic_result = texture(albedo_metallic_sampler, uv);
	vec4 albedo = vec4(albedo_metallic_result.rgb, 1.0) * object_color;
	albedo_metallic = vec4(albedo.rgb, albedo_metallic_result.a);

	vec4 normal_roughness_result = texture(normal_roughness_sampler, uv);
	vec3 normal = normalize(2.0 * normal_roughness_result.xyz - vec3(1.0));
	normal = normalize(tbn * normal);
	normal = 0.5 * normal + 0.5;
	normal_roughness = vec4(normal, normal_roughness_result.w);

	frag_pos = vec4(pos, emissive);
}
