#version 330

layout(location = 0) in vec3 vert_pos;
layout(location = 1) in vec2 vert_uv;
layout(location = 2) in vec3 vert_normal;
layout(location = 3) in vec3 vert_tangent;

out mat3 tbn;
out vec2 uv;
out vec3 pos;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

void main()
{
	vec3 bitangent = -cross(vert_tangent, vert_normal);
	bitangent = normalize(vec3(model * vec4(bitangent, 0.0)));
	vec3 normal = normalize(vec3(model * vec4(vert_normal, 0.0)));
	vec3 tangent = normalize(vec3(model * vec4(vert_tangent, 0.0)));
	tbn = mat3(tangent, bitangent, normal);

	uv = vert_uv;

	vec4 world_pos = model * vec4(vert_pos, 1.0);
	pos = world_pos.xyz;

	gl_Position = proj * view * world_pos;
}
