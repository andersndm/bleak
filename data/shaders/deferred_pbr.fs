#version 430

in vec2 uv;

out vec4 color_out;

uniform sampler2D albedo_metallic_sampler;
uniform sampler2D normal_roughness_sampler;
uniform sampler2D pos_sampler;

uniform vec3 cam_pos;

uniform vec3 ambient_light_color;

uniform int output_mode;

const float pi = 3.141592653;

struct dir_light
{
	float color_r;
	float color_g;
	float color_b;
	float dir_x;
	float dir_y;
	float dir_z;
};
uniform uint num_dir_lights;
layout (std430, binding=2) buffer dir_lights_data
{
	dir_light dlights[];
};

struct point_light
{
	float color_r;
	float color_g;
	float color_b;
	float pos_x;
	float pos_y;
	float pos_z;
};
uniform uint num_point_lights;
layout (std430, binding=3) buffer point_lights_data
{
	point_light plights[];
};

struct spot_light
{
	float color_r;
	float color_g;
	float color_b;
	float pos_x;
	float pos_y;
	float pos_z;
	float dir_x;
	float dir_y;
	float dir_z;
	float inner_cutoff;
	float outer_cutoff;
};
uniform uint num_spot_lights;
layout (std430, binding=4) buffer spot_lights_data
{
	spot_light slights[];
};

float distribution_ggx(vec3 normal, vec3 half_vec, float a)
{
	float a2 = a * a;
	float ndoth = max(dot(normal, half_vec), 0.0);
	float ndoth2 = ndoth * ndoth;
	float nom = a2;
	float denom = (ndoth2 * (a2 - 1.0) + 1.0);
	denom = pi * denom * denom;

	return nom / denom;
}

float geometry_schlick_ggx(float ndotv, float k)
{
	float nom = ndotv;
	float denom = ndotv * (1.0 - k) + k;
	return nom / denom;
}

float geometry_smith(vec3 normal, vec3 v, vec3 l, float k)
{
	float ndotv = max(dot(normal, v), 0.0);
	float ndotl = max(dot(normal, l), 0.0);
	float ggx1 = geometry_schlick_ggx(ndotv, k);
	float ggx2 = geometry_schlick_ggx(ndotl, k);

	return ggx1 * ggx2;
}

vec3 fresnel_schlick(float cos_theta, vec3 f0)
{
	return f0 + (1.0 - f0) * pow(1.0 - cos_theta, 5.0);
}

vec3 calc_dir_light(vec3 light_dir, vec3 light_color, vec3 view_dir, vec3 normal, vec3 f0,
					float roughness, float metallic, vec3 albedo)
{
	vec3 half_vec = normalize(view_dir + light_dir);
	vec3 radiance = light_color;
	vec3 f = fresnel_schlick(max(dot(half_vec, view_dir), 0.0), f0);
	float ndf = distribution_ggx(normal, half_vec, roughness);
	float g = geometry_smith(normal, view_dir, light_dir, roughness);
	vec3 numerator = ndf * g * f;
	float denominator = 4.0 * max(dot(normal, view_dir), 0.0) * max(dot(normal, light_dir), 0.0);
	vec3 specular = numerator / max(denominator, 0.001);
	vec3 ks = f;
	vec3 kd = vec3(1.0) - ks;
	kd *= (1.0 - metallic);
	float ndotl = max(dot(normal, light_dir), 0.0);
	return (kd * albedo / pi + specular) * radiance * ndotl;
}

vec3 calc_point_light(vec3 light_pos, vec3 pos, vec3 light_color, vec3 normal, vec3 view_dir,
					  vec3 f0, float roughness, float metallic, vec3 albedo)
{
	vec3 light_dir = normalize(light_pos - pos);
	vec3 half_vec = normalize(view_dir + light_dir);
	float dist = length(light_dir);
	float attenuation = 1.0 / (dist * dist);
	vec3 radiance = attenuation * light_color;
	vec3 f = fresnel_schlick(max(dot(half_vec, view_dir), 0.0), f0);
	float ndf = distribution_ggx(normal, half_vec, roughness);
	float g = geometry_smith(normal, view_dir, light_dir, roughness);
	vec3 numerator = ndf * g * f;
	float denominator = 4.0 * max(dot(normal, view_dir), 0.0) * max(dot(normal, light_dir), 0.0);
	vec3 specular = numerator / max(denominator, 0.001);
	vec3 ks = f;
	vec3 kd = vec3(1.0) - ks;
	kd *= (1.0 - metallic);
	float ndotl = max(dot(normal, light_dir), 0.0);
	return (kd * albedo / pi + specular) * radiance * ndotl;
}

vec3 calc_spot_light(vec3 light_pos, vec3 pos, vec3 sl_dir, vec3 light_color, vec3 normal, vec3 view_dir,
					 float inner, float outer, vec3 f0, float roughness, float metallic, vec3 albedo)
{
	vec3 light_dir = normalize(light_pos - pos);
	float theta = dot(light_dir, -normalize(sl_dir));
	float epsilon = inner - outer;
	float intensity = clamp((theta - outer) / epsilon, 0.0, 1.0);
	return intensity * calc_point_light(light_pos, pos, light_color, normal, view_dir, f0,
									    roughness, metallic, albedo);
}

void main()
{
	vec4 albedo_metallic_result = texture(albedo_metallic_sampler, uv);
	vec4 normal_roughness_result = texture(normal_roughness_sampler, uv);
	vec4 pos_result = texture(pos_sampler, uv);

	vec3 albedo = pow(albedo_metallic_result.rgb, vec3(2.2));
	vec3 normal = normalize(2.0 * normal_roughness_result.xyz - 1.0);
	vec3 pos = pos_result.xyz;
	float emissive = floatBitsToUint(pos_result.w);
	float metallic = albedo_metallic_result.a;
	float roughness = normal_roughness_result.w;

	vec3 view_dir = normalize(cam_pos - pos);

	if (output_mode == 1)
	{
		color_out = vec4(albedo_metallic_result.rgb, 1.0);
		return;
	}
	else if (output_mode == 2)
	{
		color_out = vec4(normal.xyz, 1.0);
		return;
	}
	else if (output_mode == 3)
	{
		color_out = vec4(pos_result.xyz, 1.0);
		return;
	}
	else if (output_mode == 4)
	{
		color_out = vec4(metallic, metallic, metallic, 1.0);
		return;
	}
	else if (output_mode == 5)
	{
		color_out = vec4(roughness, roughness, roughness, 1.0);
		return;
	}
	else if (output_mode == 6)
	{
		color_out = vec4(emissive, emissive, emissive, 1.0);
		return;
	}

	vec3 color = vec3(0.0f);
	if (emissive > 0.0f)
	{
		color = albedo;
	}
	else
	{
		float spec_pwr = 16.0;
		float spec_str = 2.0;

		vec3 f0 = vec3(0.04);
		f0 = mix(f0, albedo, metallic);
		vec3 lo = vec3(0.0);
		for (uint dlight_idx = 0; dlight_idx < num_dir_lights; ++dlight_idx)
		{
			dir_light dlight = dlights[dlight_idx];
			vec3 light_dir = normalize(-vec3(dlight.dir_x, dlight.dir_y, dlight.dir_z));
			vec3 light_color = 0.1 * vec3(dlight.color_r, dlight.color_g, dlight.color_b);

			lo += calc_dir_light(light_dir, light_color, view_dir, normal, f0,
					roughness, metallic, albedo);
		}

		for (uint plight_idx = 0; plight_idx < num_point_lights; ++plight_idx)
		{
			point_light plight = plights[plight_idx];
			vec3 light_pos = vec3(plight.pos_x, plight.pos_y, plight.pos_z);
			vec3 light_color = vec3(plight.color_r, plight.color_g, plight.color_b);

			lo += calc_point_light(light_pos, pos, light_color, normal, view_dir, f0, roughness,
								   metallic, albedo);
		}

		for (uint slight_idx = 0; slight_idx < num_spot_lights; ++slight_idx)
	    {
			spot_light slight = slights[slight_idx];
			vec3 sl_dir = vec3(slight.dir_x, slight.dir_y, slight.dir_z);
			vec3 light_pos = vec3(slight.pos_x, slight.pos_y, slight.pos_z);
			vec3 light_color = vec3(slight.color_r, slight.color_g, slight.color_b);
			lo += calc_spot_light(light_pos,pos,sl_dir, light_color, normal, view_dir,
								  slight.inner_cutoff, slight.outer_cutoff, f0, roughness,
								  metallic, albedo);
		}

		color = (albedo * ambient_light_color) + lo;
	}


	vec3 hdr_color = color / (color + vec3(1.0));
	float exposure = 1.0;
	vec3 mapped = vec3(1.0) - exp(-hdr_color * exposure);
	vec3 gamma_corrected_color = pow(mapped, vec3(0.45454545));	// 0.45454545 ~ 1/2.2
	color_out = vec4(gamma_corrected_color, 1.0);
	/*
	color_out = vec4(color, 1.0);
	*/
}
