#define AST_IMPLEMENTATION
#define AST_IGNORE_TIME
#include <ast.h>
#include <apl.h>
#include <apl_gl.h>

#include "platform.h"
#include "host.h"

apl_core apl;

void engine_audio_callback(struct apl_audio_request *request)
{
	for (s16 *sample = request->sample; sample != request->end_sample;)
	{
		for (
				u32 channel_idx = 0;
				channel_idx < request->channels;
				++channel_idx
			)
		{
			*sample++ = 0;
		}
	}
}

void update_game_button(
		game_button_state *state,
		struct apl_digital_button *button
	)
{
	state->down = button->down;
	state->pressed = button->pressed;
	state->released = button->released;
}


void update_input(game_input *input)
{
	input->dt_frame = 0.0167f;
	// #todo update analog properly
	input->analog = apl.input.analog;

	update_game_button(&input->left_click, &apl.input.mouse.left_button);
	update_game_button(&input->right_click, &apl.input.mouse.right_button);
	input->mouse_x = apl.input.mouse.position.x;
	input->mouse_y = apl.input.mouse.position.y;
	input->d_mouse_x = apl.input.mouse.delta_position.x;
	input->d_mouse_y = apl.input.mouse.delta_position.y;
	input->mouse_scroll = apl.input.mouse.delta_wheel;

	update_game_button(&input->f1, &apl.input.keys[AK_F1]);
	update_game_button(&input->f2, &apl.input.keys[AK_F2]);
	update_game_button(&input->f3, &apl.input.keys[AK_F3]);
	update_game_button(&input->f4, &apl.input.keys[AK_F4]);
	update_game_button(&input->f5, &apl.input.keys[AK_F5]);
	update_game_button(&input->f6, &apl.input.keys[AK_F6]);
	update_game_button(&input->f7, &apl.input.keys[AK_F7]);

	update_game_button(&input->lshift, &apl.input.keys[AK_SHIFT_LEFT]);

	f32 tmp1 = apl.input.keys[AK_D].down ? 1.0f : 0.0f;
	f32 tmp2 = apl.input.keys[AK_A].down ? 1.0f : 0.0f;
	input->keyboard.left_horizontal_axis = tmp1 - tmp2;

	tmp1 = apl.input.keys[AK_W].down ? 1.0f : 0.0f;
	tmp2 = apl.input.keys[AK_S].down ? 1.0f : 0.0f;
	input->keyboard.left_vertical_axis = tmp1 - tmp2;

	// #todo right axes?
	input->keyboard.right_horizontal_axis = (f32)input->d_mouse_x * 0.1f;
	input->keyboard.right_vertical_axis = (f32)input->d_mouse_y * -0.1f;

	update_game_button(&input->keyboard.up, &apl.input.keys[AK_W]);
	update_game_button(&input->keyboard.down, &apl.input.keys[AK_S]);
	update_game_button(&input->keyboard.left, &apl.input.keys[AK_A]);
	update_game_button(&input->keyboard.right, &apl.input.keys[AK_D]);

	update_game_button(&input->keyboard.action_up, &apl.input.keys[AK_UP]);
	update_game_button(&input->keyboard.action_down, &apl.input.keys[AK_DOWN]);
	update_game_button(&input->keyboard.action_left, &apl.input.keys[AK_LEFT]);
	update_game_button(&input->keyboard.action_right, &apl.input.keys[AK_RIGHT]);

	update_game_button(&input->keyboard.left_shoulder, &apl.input.keys[AK_Q]);
	update_game_button(&input->keyboard.right_shoulder, &apl.input.keys[AK_E]);

	update_game_button(&input->keyboard.start, &apl.input.keys[AK_SPACE]);
	update_game_button(&input->keyboard.back, &apl.input.keys[AK_ESCAPE]);

	input->gamepad.left_horizontal_axis = apl.input.gamepad.left_stick.x;
	input->gamepad.left_vertical_axis = apl.input.gamepad.left_stick.y;
	input->gamepad.right_horizontal_axis = apl.input.gamepad.right_stick.x;
	input->gamepad.right_vertical_axis = apl.input.gamepad.right_stick.y;

	update_game_button(&input->gamepad.up, &apl.input.gamepad.left_stick.up);
	update_game_button(&input->gamepad.down, &apl.input.gamepad.left_stick.down);
	update_game_button(&input->gamepad.left, &apl.input.gamepad.left_stick.left);
	update_game_button(&input->gamepad.right, &apl.input.gamepad.left_stick.right);

	update_game_button(&input->gamepad.action_up, &apl.input.gamepad.y);
	update_game_button(&input->gamepad.action_down, &apl.input.gamepad.a);
	update_game_button(&input->gamepad.action_left, &apl.input.gamepad.x);
	update_game_button(&input->gamepad.action_right, &apl.input.gamepad.b);

	update_game_button(&input->gamepad.left_shoulder, &apl.input.gamepad.left_shoulder);
	update_game_button(&input->gamepad.right_shoulder, &apl.input.gamepad.right_shoulder);

	update_game_button(&input->gamepad.start, &apl.input.gamepad.start);
	update_game_button(&input->gamepad.back, &apl.input.gamepad.back);
}

s32 main(s32 argc, char **argv)
{
	if (argc > 2)
	{
		// #todo arguments
		char *path = *argv;
		printf("%s\n", path);
	}

#ifdef PL_WIN
	void *gamelib = ast_load_shared_library("bleak");
#else
	void *gamelib = ast_load_shared_library("./libbleak");
#endif
	if (!gamelib)
	{
		ast_print_color(CCOLOR_RED, 1);
		printf("error: ");
		ast_print_color(CCOLOR_RESET, 0);
		printf("failed to load engine shared library\n");
		return 1;
	}

	HOST_FRAME host_frame_ptr = (HOST_FRAME)ast_load_function(
			gamelib,
			"host_frame"
	);
	if (!host_frame_ptr)
	{
		ast_print_color(CCOLOR_RED, 1);
		printf("error: ");
		ast_print_color(CCOLOR_RESET, 0);
		printf("failed to load host_frame_ptr\n");
		return 1;
	}

	apl.window.position.x = APL_WND_DEFAULT_POS;
	apl.window.position.y = APL_WND_DEFAULT_POS;

#if 0
	apl.window.size.x = 1920;
	apl.window.size.y = 1080;
#else
	apl.window.size.x = 1280;
	apl.window.size.y = 720;
#endif

	apl.window.style = APL_WND_DEFAULT;
	apl.window.title = "hello world";

	apl.audio.callback = engine_audio_callback;


	if (!apl_initialize(&apl))
	{
		ast_print_color(CCOLOR_RED, 1);
		printf("error: ");
		ast_print_color(CCOLOR_RESET, 0);
		printf("failed to initialize the platform layer\n");
		return 1;
	}

	game_memory memory = { 0 };
	memory.storage_size = MEGABYTES(256);
	memory.storage = ast_calloc(1, memory.storage_size);
	if(!memory.storage)
	{
		printf(
				"Failed to allocate required memory.\n%llu bytes\n",
				(unsigned long long)memory.storage_size
		);
		return 1;
	}

	apl_gl gl;
	game_input input;
	apl_load_opengl_functions(&gl);

	while (!apl.quit)
	{
		apl_pull(&apl);

		game_state *state = memory.storage;
		state->sys.client_width = apl.window.size.x;
		state->sys.client_height = apl.window.size.y;
		update_input(&input);

		if (!host_frame_ptr(&memory, &input, &gl))
		{
			apl.quit = true;
		}

		apl.input.mouse.center_cursor = input.center_cursor;

		apl_push(&apl);
	}

	ast_free(memory.storage);

	return 0;
}
