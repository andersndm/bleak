#ifndef BLEAK_PLATFORM_H
#define BLEAK_PLATFORM_H

#define AST_IGNORE_TIME
#define AST_IMPLEMENTATION
#include <ast.h>

#ifndef _WIN32
#include <unistd.h>
#else
#include <Windows.h>
#endif

#include <ast_math.h>

#include <string.h>

void fatal(void)
{
	ast_print_color(CCOLOR_RED, 1);
	printf("fatal error\n");
	ast_print_color(CCOLOR_RESET, 0);
	exit(1);
}

typedef struct game_memory
{
	u64 storage_size;
	void* storage;
} game_memory;

typedef struct game_button_state
{
	b8 down;
	b8 pressed;
	b8 released;
} game_button_state;

typedef struct input_controller
{
	f32 left_horizontal_axis;
	f32 left_vertical_axis;
	f32 right_horizontal_axis;
	f32 right_vertical_axis;

	union
	{
		game_button_state buttons[12];
		struct
		{
			game_button_state up;
			game_button_state down;
			game_button_state left;
			game_button_state right;

			game_button_state action_up;
			game_button_state action_down;
			game_button_state action_left;
			game_button_state action_right;

			game_button_state left_shoulder;
			game_button_state right_shoulder;

			game_button_state start;
			game_button_state back;

			// NOTE(anders): All buttons must come before the terminator, which
			// is used for looping over all the buttons
			game_button_state terminator;
		};
	};
} input_controller;

typedef struct game_input
{
	f32 dt_frame;
	b32 analog;

	game_button_state left_click;
	game_button_state right_click;
	s32 mouse_x, mouse_y;
	s32 d_mouse_x, d_mouse_y;
	s32 mouse_scroll;
	b32 center_cursor;

	input_controller keyboard;
	input_controller gamepad;

	// debug buttons
	game_button_state f1;
	game_button_state f2;
	game_button_state f3;
	game_button_state f4;
	game_button_state f5;
	game_button_state f6;
	game_button_state f7;

	game_button_state lshift;
} game_input;

typedef struct apl_gl apl_gl;
typedef b32 (*HOST_FRAME)(game_memory *memory, game_input *input, apl_gl *gl);

#endif// !BLEAK_PLATFORM_H
