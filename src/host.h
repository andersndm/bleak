#ifndef HOST_H
#define HOST_H

#include "platform.h"

#include <ast_math.h>
#include <apl_png.h>

#include "assets.h"
#include "render.h"
#include "entity.h"

typedef struct system_state
{
	s32 client_width;
	s32 client_height;
	b32 quit;
} system_state;

typedef struct game_state
{
	b8 initialized;
	u64 num_frames;
	f32 time_elapsed_since_start;
	f32 current_time_elapsed;

	deferred_renderer renderer;

	u32 num_entities;
	entity entities[MAX_ENTITIES];

	game_assets assets;

	system_state sys;
} game_state;

#endif// !HOST_H
