/* #todo:
	- Should the position buffer take dielectric reflectivity in it's alpha
	component, or emmisivity? Is emmisivity even a thing in this renderer?

	- Checkout HDR mapping more when scenes are constructed to test it properly

	- Image based lighting

	- Transparency

	- Model loading (fbx?) ... animation

	- Shadows (csm)

	- Blur (and other post-processing)

	- Frustum culling ??

	- SSAO works on windows, but not on linux......

	- re-design the structure of the renderer so that the pushing
      render instructions makes more sense. Make it so that you can
      push to textures, like the framebuffer, and combine in more
      disparate ways
*/

#include <apl_gl.h>

mat4 matrix_from_transform(transform3d transform)
{
	// #debug ensure order and multiplications are correct
	mat4 trans = m4_translation(transform.position);
	mat4 scale = m4_scale(transform.scale);
	mat4 result = m4_mul_m4(trans, m4_mul_m4(scale, m4_rotation_quaternion(transform.rotation)));
	return result;
}

// #todo better function name, just setting the state for 3d rendering. There may be differences
// for 2d hud rendering or similar
void set_gl_state_3d(void)
{
	// #todo there is really no reason to have this here, if it is needed it would be elsewhere,
	// as the renderer plays no part in setting up opengl
	const unsigned char* gl_version = gl.get_string(GL_VERSION);
	printf("OpenGL Version: %s\n", gl_version);

	gl.enable(GL_CULL_FACE);
	gl.front_face(GL_CCW);
	gl.cull_face(GL_BACK);
	gl.enable(GL_DEPTH_TEST);
	gl.depth_func(GL_LESS);
}

void init_skybox_mesh(deferred_renderer *renderer)
{
	f32 a = 1500.0f;
	//f32 a = 1.0f;
	f32 pos_data[] =
	{
		// front face
		-a, -a, -a, // 0
		 a,  a, -a, // 1
		-a,  a, -a, // 2
		 a, -a, -a, // 3

		// rear face
		 a, -a,  a, // 4
		-a,  a,  a, // 5
		 a,  a,  a, // 6
		-a, -a,  a, // 7

		// left face
		-a, -a,  a, // 8
		-a,  a, -a, // 9
		-a,  a,  a, // 10
		-a, -a, -a, // 11

		// right face
		 a, -a, -a, // 12
		 a,  a,  a, // 13
		 a,  a, -a, // 14
		 a, -a,  a, // 15

		// top face
		-a,  a, -a, // 16
		 a,  a,  a, // 17
		-a,  a,  a, // 18
		 a,  a, -a, // 19

		// bottom face
		-a, -a,  a, // 20
		 a, -a, -a, // 21
		-a, -a, -a, // 22
		 a, -a,  a  // 23
	};

	f32 offset = 0.75f;
	f32 uv_data[] =
	{
		// side - front
		offset + 0.005f, 0.500f,
		offset + 0.245f, 0.250f,
		offset + 0.005f, 0.250f,
		offset + 0.245f, 0.500f,

		// side - rear
		offset + 0.005f, 0.500f,
		offset + 0.245f, 0.250f,
		offset + 0.005f, 0.250f,
		offset + 0.245f, 0.500f,

		// side - left
		offset + 0.005f, 0.500f,
		offset + 0.245f, 0.250f,
		offset + 0.005f, 0.250f,
		offset + 0.245f, 0.500f,

		// side - left
		offset + 0.005f, 0.500f,
		offset + 0.245f, 0.250f,
		offset + 0.005f, 0.250f,
		offset + 0.245f, 0.500f,

		// top
		offset + 0.005f, 0.250f,
		offset + 0.245f, 0.005f,
		offset + 0.005f, 0.005f,
		offset + 0.245f, 0.250f,

		// bottom
		offset + 0.005f, 0.745f,
		offset + 0.245f, 0.500f,
		offset + 0.005f, 0.500f,
		offset + 0.245f, 0.745f
	};

	f32 nt_data[ARRAY_COUNT(pos_data)] = { 0.0f };

	u32 index_data[] =
	{
		// front face
		0, 1, 2,
		0, 3, 1,
		// rear face
		4, 5, 6,
		4, 7, 5,
		// left face
		8, 9, 10,
		8, 11, 9,
		// right face
		12, 13, 14,
		12, 15, 13,
		// top face
		16, 17, 18,
		16, 19, 17,
		// bottom face
		20, 21, 22,
		20, 23, 21
	};

	// mesh_id contains vao, ibo, num_indices
	renderer->skybox_mesh.type = RENDERMESH_TRIANGLES;
	renderer->skybox_mesh.flags = 0; // none needed for skybox rendering
	renderer->skybox_mesh.color = v3(1.0f, 1.0f, 1.0f);
	zero_memory(&renderer->skybox_mesh.diffuse, sizeof(texture_id));
	zero_memory(&renderer->skybox_mesh.normal, sizeof(texture_id));
	renderer->skybox_mesh.model = m4_identity();

	u32 vao, vbo, uvbo, nbo, tbo, ibo;
	gl.gen_vertex_arrays(1, &vao);
	gl.bind_vertex_array(vao);

	gl.gen_buffers(1, &vbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, vbo);
	gl.buffer_data(GL_ARRAY_BUFFER, sizeof(f32) * ARRAY_COUNT(pos_data), pos_data, GL_STATIC_DRAW);
	gl.enable_vertex_attrib_array(0);
	gl.vertex_attrib_pointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);

	gl.gen_buffers(1, &uvbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, uvbo);
	gl.buffer_data(GL_ARRAY_BUFFER, sizeof(f32) * ARRAY_COUNT(uv_data), uv_data, GL_STATIC_DRAW);
	gl.enable_vertex_attrib_array(1);
	gl.vertex_attrib_pointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);

	gl.gen_buffers(1, &nbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, nbo);
	gl.buffer_data(GL_ARRAY_BUFFER, sizeof(f32) * ARRAY_COUNT(nt_data), nt_data, GL_STATIC_DRAW);
	gl.enable_vertex_attrib_array(2);
	gl.vertex_attrib_pointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);

	gl.gen_buffers(1, &tbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, tbo);
	gl.buffer_data(GL_ARRAY_BUFFER, sizeof(f32) * ARRAY_COUNT(nt_data), nt_data, GL_STATIC_DRAW);
	gl.enable_vertex_attrib_array(3);
	gl.vertex_attrib_pointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);

	gl.bind_vertex_array(0);
	gl.bind_buffer(GL_ARRAY_BUFFER, 0);

	gl.gen_buffers(1, &ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	gl.buffer_data(GL_ELEMENT_ARRAY_BUFFER, sizeof(u32) * ARRAY_COUNT(index_data),
				   index_data, GL_STATIC_DRAW);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	renderer->skybox_mesh.mesh_id.asset_index = 0;
	renderer->skybox_mesh.mesh_id.vao = vao;
	renderer->skybox_mesh.mesh_id.ibo = ibo;
	renderer->skybox_mesh.mesh_id.num_indices = ARRAY_COUNT(index_data);

	CHECK_GL_ERROR;
}

void init_deferred_renderer(
		deferred_renderer *renderer,
		mesh_id render_plane,
		glshader *skybox_shader,
		glshader *gbuffer_shader,
		glshader *deferred_shader,
		u32 viewport_width,
		u32 viewport_height,
		u32 internal_render_width,
		u32 internal_render_height
	)
{
	memory_arena_init(&renderer->arena, MEGABYTES(2));

	// #note: render plane
	renderer->render_plane = render_plane;
	// #todo do these need to be stored? Probably not
	renderer->viewport_width = viewport_width;
	renderer->viewport_height = viewport_height;
	renderer->internal_render_width = internal_render_width;
	renderer->internal_render_height = internal_render_height;

	renderer->skybox_shader = skybox_shader;
	renderer->gbuffer_shader = gbuffer_shader;
	renderer->deferred_shader = deferred_shader;

	renderer->output_mode = RENDEROUT_COMPLETE;
	renderer->render_3d_scene = true;

	init_skybox_mesh(renderer);

	// #note: depth framebuffer
	// #important: Depth must come first otherwise it doesn't work

	// #note: Non-depth gbuffer framebuffer
	gl.gen_framebuffers(1, &renderer->gb_framebuffer);
	gl.bind_framebuffer(GL_FRAMEBUFFER, renderer->gb_framebuffer);

	// #note: albedo and metallic texture
	gl.gen_textures(1, &renderer->gb_albedo_metallic_texture);
	gl.bind_texture(GL_TEXTURE_2D, renderer->gb_albedo_metallic_texture);
	gl.tex_image_2d(
			GL_TEXTURE_2D,
			0,
			GL_RGBA,
			renderer->internal_render_width,
			renderer->internal_render_height,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			0
	);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	gl.framebuffer_texture(
			GL_FRAMEBUFFER,
			GL_COLOR_ATTACHMENT0,
			renderer->gb_albedo_metallic_texture,
			0
	);

	// #note: normal and roughness texture
	gl.gen_textures(1, &renderer->gb_normal_roughness_texture);
	gl.bind_texture(GL_TEXTURE_2D, renderer->gb_normal_roughness_texture);
	gl.tex_image_2d(
			GL_TEXTURE_2D,
			0,
			GL_RGBA,
			renderer->internal_render_width,
			renderer->internal_render_height,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			0
	);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	gl.framebuffer_texture(
			GL_FRAMEBUFFER,
			GL_COLOR_ATTACHMENT1,
			renderer->gb_normal_roughness_texture,
			0
	);

	// #note: position texture
	// #todo: currently has a spare 16F alpha channel
	gl.gen_textures(1, &renderer->gb_position_texture);
	gl.bind_texture(GL_TEXTURE_2D, renderer->gb_position_texture);
	gl.tex_image_2d(
			GL_TEXTURE_2D,
			0,
			GL_RGBA16F,
			renderer->internal_render_width,
			renderer->internal_render_height,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			0
	);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	gl.framebuffer_texture(
			GL_FRAMEBUFFER,
			GL_COLOR_ATTACHMENT2,
			renderer->gb_position_texture,
			0
	);

	// #note do not (currently) need an external reference to the depth buffer
	u32 gb_depth_buffer = 0;
	gl.gen_renderbuffers(1, &gb_depth_buffer);
	gl.bind_renderbuffer(GL_RENDERBUFFER, gb_depth_buffer);
	gl.renderbuffer_storage(
			GL_RENDERBUFFER,
			GL_DEPTH_COMPONENT,
			renderer->internal_render_width,
			renderer->internal_render_height
	);
	gl.framebuffer_renderbuffer(
			GL_FRAMEBUFFER,
			GL_DEPTH_ATTACHMENT,
			GL_RENDERBUFFER,
			gb_depth_buffer
	);

	GLenum draw_buffers[3] =
	{
		GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2
	};
	gl.draw_buffers(3, draw_buffers);

	if(gl.check_framebuffer_status(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("gbuffer frame buffer status not complete.\n");
		DEBUG_BREAK;
		exit(0);
	}

	CHECK_GL_ERROR;
}

void render_push_clear(deferred_renderer *renderer, vec3 color)
{
	render_instruction inst = RENDERINST_CLEAR;
	memory_arena_push(&renderer->arena, &inst, sizeof(render_instruction));
	memory_arena_push(&renderer->arena, &color, sizeof(vec3));
}

void render_push_ambient_color(deferred_renderer *renderer, vec3 color)
{
	renderer->ambient.color_r = color.r;
	renderer->ambient.color_g = color.g;
	renderer->ambient.color_b = color.b;
}

void render_push_directional_lamp(
		deferred_renderer *renderer,
		quaternion rot,
		vec3 color,
		f32 intensity
	)
{
	ASSERT(renderer->num_dir_lights <= ARRAY_COUNT(renderer->dir_lights));

	vec3 dirs = v3_rotate(rot, v3(0.0f, -1.0f, 0.0f));

	dir_light *light = renderer->dir_lights + renderer->num_dir_lights++;
	light->color_r = color.r * intensity;
	light->color_g = color.g * intensity;
	light->color_b = color.b * intensity;
	light->dir_x = dirs.x;
	light->dir_y = dirs.y;
	light->dir_z = dirs.z;
}

void render_push_point_lamp(deferred_renderer *renderer, vec3 pos, vec3 color, f32 intensity)
{
	ASSERT(renderer->num_point_lights <= ARRAY_COUNT(renderer->point_lights));

	point_light *light = renderer->point_lights + renderer->num_point_lights++;
	light->color_r = color.r * intensity;
	light->color_g = color.g * intensity;
	light->color_b = color.b * intensity;
	light->pos_x = pos.x;
	light->pos_y = pos.y;
	light->pos_z = pos.z;
}

void render_push_spot_lamp(
		deferred_renderer *renderer,
		vec3 pos,
		quaternion rot,
		vec3 color,
		f32 intensity,
		f32 inner_cutoff,
		f32 outer_cutoff
	)
{
	ASSERT(renderer->num_spot_lights <= ARRAY_COUNT(renderer->spot_lights));

	vec3 dirs = v3_rotate(rot, v3(0.0f, -1.0f, 0.0f));

	spot_light *light = renderer->spot_lights + renderer->num_spot_lights++;
	light->color_r = color.r * intensity;
	light->color_g = color.g * intensity;
	light->color_b = color.b * intensity;
	light->pos_x = pos.x;
	light->pos_y = pos.y;
	light->pos_z = pos.z;
	light->dir_x = dirs.x;
	light->dir_y = dirs.y;
	light->dir_z = dirs.z;
	light->inner_cutoff = inner_cutoff;
	light->outer_cutoff = outer_cutoff;
}

void render_push_mesh(
		deferred_renderer *renderer,
		render_mesh_type type,
		u16 flags,
		vec3 color,
		texture_id diffuse,
		texture_id normal,
		mesh_id mesh_id,
		transform3d transform
	)
{
	render_instruction inst = RENDERINST_MESH;

	render_mesh rmesh = { 0 };
	rmesh.type = type;
	rmesh.flags = flags;
	rmesh.color = color;
	rmesh.diffuse = diffuse;
	rmesh.normal = normal;
	rmesh.mesh_id = mesh_id;
	rmesh.model = matrix_from_transform(transform);

	memory_arena_push(&renderer->arena, &inst, sizeof(render_instruction));
	memory_arena_push(&renderer->arena, &rmesh, sizeof(render_mesh));
}

void render_push_active_camera(deferred_renderer *renderer, camera_3d camera, vec3 pos)
{
	renderer->projection = camera.proj;
	renderer->view = camera.view;
	renderer->cam_pos = pos;
}

void render_push_skybox(deferred_renderer *renderer, game_asset *asset)
{
	//ASSERT(asset->type == ASSETTYPE_CUBE_MAP);
	ASSERT(asset->type == ASSETTYPE_TEXTURE);
	renderer->skybox = asset;
}

void deferred_renderer_draw(deferred_renderer *renderer)
{
#if 1
	CHECK_GL_ERROR;

	// #todo the m4_identity is only there in case world warping is desired, in which case a
	// world matrix will be used there.
	// #todo is mpv needed? It is currently not being referenced
	// mat4 mvp = m4_mul_m4(renderer->projection, m4_mul_m4(renderer->view, m4_identity()));

	if (renderer->render_3d_scene)
	{
#if 0
		if (renderer->skybox)
		{
			gl.use_program(renderer->skybox_shader->program);
			gl.bind_framebuffer(GL_FRAMEBUFFER, renderer->gb_framebuffer);
			gl.clear_color(1.0f, 0.0f, 1.0f, 1.0f);
			gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		

			gl.uniform_m4fv(
				renderer->skybox_shader->gbuffer.projection_id,
				1,
				GL_TRUE,
				(float*)(&renderer->projection.e)
				);
			mat4 view = renderer->view;
#if 0
			view.a41 = 0.0f;
			view.a42 = 0.0f;
			view.a43 = 0.0f;
#endif
			gl.uniform_m4fv(
				renderer->skybox_shader->gbuffer.view_id,
				1,
				GL_TRUE,
				(float*)(&view.e)
				);
			gl.uniform4f(
				renderer->skybox_shader->skybox.color_id,
				renderer->skybox_mesh.color.r,
				renderer->skybox_mesh.color.g,
				renderer->skybox_mesh.color.b,
				1.0f);

			CHECK_GL_ERROR;

			gl.uniform1i(renderer->skybox_shader->skybox.albedo_sampler_id, 0);
//			gl.active_texture(GL_TEXTURE0 + 0);
			gl.bind_texture(GL_TEXTURE_CUBE_MAP, renderer->skybox->cubemap.cubemap_id);
				
			CHECK_GL_ERROR;
			gl.bind_vertex_array(renderer->skybox_mesh.mesh_id.vao);
			gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, renderer->skybox_mesh.mesh_id.ibo);

			CHECK_GL_ERROR;

			gl.draw_elements(
				GL_TRIANGLES,
				renderer->skybox_mesh.mesh_id.num_indices,
				GL_UNSIGNED_INT,
				(void *)0
				);
			CHECK_GL_ERROR;
				
			gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			gl.bind_vertex_array(0);
			gl.bind_texture(GL_TEXTURE_2D, 0);
			gl.active_texture(GL_TEXTURE0 + 0);
			gl.bind_texture(GL_TEXTURE_2D, 0);

			CHECK_GL_ERROR;
		}
#endif
				
		// #note: gbuffer pass
		gl.use_program(renderer->gbuffer_shader->program);
		gl.bind_framebuffer(GL_FRAMEBUFFER, renderer->gb_framebuffer);
		if (!renderer->skybox)
		{
			gl.clear_color(0.0f, 0.0f, 0.0f, 1.0f);
			gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
		gl.uniform1i(renderer->gbuffer_shader->gbuffer.albedo_metallic_sampler_id, 0);
		gl.uniform1i(renderer->gbuffer_shader->gbuffer.normal_roughness_sampler_id, 1);
		gl.uniform_m4fv(
			renderer->gbuffer_shader->gbuffer.projection_id,
			1,
			GL_TRUE,
			(float*)(&renderer->projection.e)
			);
		gl.uniform_m4fv(
			renderer->gbuffer_shader->gbuffer.view_id,
			1,
			GL_TRUE,
			(float*)(&renderer->view.e)
			);
#if 1
		gl.clear_color(0.0f, 0.0f, 0.0f, 1.0f);
		gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		

		if (renderer->skybox)
		{
			// #todo if any scenes start to become large enough it may
			// be best to translate the skybox with the camera
#if 1
			mat4 skybox_model = m4_translation(renderer->cam_pos);
#else
			mat4 skybox_model = m4_translation(v3(0.0f, 0.0f, 0.0f));
#endif
			gl.uniform_m4fv(renderer->gbuffer_shader->gbuffer.model_id,
							1,
							GL_TRUE,
							(float*)(&skybox_model.e)
				);

			CHECK_GL_ERROR;
			// #todo render_mesh and object color may want to have an alpha channel if objects
			// will end up fading, such as particle systems, etc.
			gl.uniform4f(
				renderer->gbuffer_shader->gbuffer.object_color_id,
				renderer->skybox_mesh.color.r,
				renderer->skybox_mesh.color.g,
				renderer->skybox_mesh.color.b,
				1.0f
				);

			CHECK_GL_ERROR;

			gl.uniform1f(renderer->gbuffer_shader->gbuffer.emissive_id, 1.0f);
				
			CHECK_GL_ERROR;

			gl.active_texture(GL_TEXTURE0 + 0);
			gl.bind_texture(GL_TEXTURE_2D, renderer->skybox->texture.texture_id);
				
			CHECK_GL_ERROR;
			gl.bind_vertex_array(renderer->skybox_mesh.mesh_id.vao);
			gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, renderer->skybox_mesh.mesh_id.ibo);

			CHECK_GL_ERROR;

			gl.draw_elements(
				GL_TRIANGLES,
				renderer->skybox_mesh.mesh_id.num_indices,
				GL_UNSIGNED_INT,
				(void *)0
				);
			CHECK_GL_ERROR;
				
			gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			gl.bind_vertex_array(0);
			gl.bind_texture(GL_TEXTURE_2D, 0);
			gl.active_texture(GL_TEXTURE0 + 0);
			gl.bind_texture(GL_TEXTURE_2D, 0);

			CHECK_GL_ERROR;			
		}
#endif
		
		u8 *render_data_ptr = renderer->arena.data;
		while (render_data_ptr < (u8 *)renderer->arena.data + renderer->arena.current_size)
		{
			render_instruction inst = *(render_instruction *)render_data_ptr;
			render_data_ptr += sizeof(render_instruction);
			switch (inst)
			{
			case RENDERINST_CLEAR:
			{
				vec3 color = *(vec3 *)render_data_ptr;
				render_data_ptr += sizeof(vec3);
#if 0
				gl.clear_color(color.r, color.g, color.b, 1.0f);
				gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
#endif
			} break;
			case RENDERINST_MESH:
			{
				render_mesh rmesh = *(render_mesh *)render_data_ptr;
				render_data_ptr += sizeof(render_mesh);


				CHECK_GL_ERROR;
				gl.uniform_m4fv(renderer->gbuffer_shader->gbuffer.model_id,
								1,
								GL_TRUE,
								(float*)(&rmesh.model.e)
					);

				CHECK_GL_ERROR;
				// #todo render_mesh and object color may want to have an alpha channel if objects
				// will end up fading, such as particle systems, etc.
				gl.uniform4f(
					renderer->gbuffer_shader->gbuffer.object_color_id,
					rmesh.color.r,
					rmesh.color.g,
					rmesh.color.b,
					1.0f
					);

				CHECK_GL_ERROR;

				if (rmesh.flags & RENDERFLAGS_SHADELESS)
				{
					gl.uniform1f(renderer->gbuffer_shader->gbuffer.emissive_id, 1.0f);
				}
				else
				{
					gl.uniform1f(renderer->gbuffer_shader->gbuffer.emissive_id, 0.0f);
				}

				CHECK_GL_ERROR;

				gl.active_texture(GL_TEXTURE0 + 0);
				gl.bind_texture(GL_TEXTURE_2D, rmesh.diffuse.tex_id);
				gl.active_texture(GL_TEXTURE0 + 1);
				gl.bind_texture(GL_TEXTURE_2D, rmesh.normal.tex_id);

				CHECK_GL_ERROR;
				gl.bind_vertex_array(rmesh.mesh_id.vao);
				gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, rmesh.mesh_id.ibo);

				CHECK_GL_ERROR;
				if (rmesh.type == RENDERMESH_LINES)
				{
					gl.draw_elements(
						GL_LINES,
						rmesh.mesh_id.num_indices,
						GL_UNSIGNED_INT,
						(void *)0
						);
					CHECK_GL_ERROR;
				}
				else
				{
					ASSERT(rmesh.type == RENDERMESH_TRIANGLES);
					gl.draw_elements(
						GL_TRIANGLES,
						rmesh.mesh_id.num_indices,
						GL_UNSIGNED_INT,
						(void *)0
						);
					CHECK_GL_ERROR;
				}

				// last to trigger
				CHECK_GL_ERROR;

				gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);
				gl.bind_vertex_array(0);
				gl.bind_texture(GL_TEXTURE_2D, 0);
				gl.active_texture(GL_TEXTURE0 + 0);
				gl.bind_texture(GL_TEXTURE_2D, 0);

				CHECK_GL_ERROR;

			} break;
			default:
			{
				ASSERT(0);
			}
			}
		}

		gl.bind_framebuffer(GL_FRAMEBUFFER, 0);
		gl.use_program(0);

		// #note: light pass/shading pass
		gl.clear_color(1.0f, 0.0f, 1.0f, 1.0f); // #note: color to ensure correct screen filling
		gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		gl.use_program(renderer->deferred_shader->program);

		gl.uniform1i(renderer->deferred_shader->deferred.output_id, renderer->output_mode);

		gl.uniform1i(renderer->deferred_shader->deferred.albedo_metallic_sampler_id, 0);
		gl.active_texture(GL_TEXTURE0 + 0);
		gl.bind_texture(GL_TEXTURE_2D, renderer->gb_albedo_metallic_texture);

		gl.uniform1i(renderer->deferred_shader->deferred.normal_roughness_sampler_id, 1);
		gl.active_texture(GL_TEXTURE0 + 1);
		gl.bind_texture(GL_TEXTURE_2D, renderer->gb_normal_roughness_texture);

		gl.uniform1i(renderer->deferred_shader->deferred.pos_sampler_id, 2);
		gl.active_texture(GL_TEXTURE0 + 2);
		gl.bind_texture(GL_TEXTURE_2D, renderer->gb_position_texture);

		gl.uniform3f(
			renderer->deferred_shader->deferred.camera_pos_id,
			renderer->cam_pos.x,
			renderer->cam_pos.y,
			renderer->cam_pos.z
			);

		gl.uniform3f(
			renderer->deferred_shader->deferred.ambient_light_id,
			renderer->ambient.color_r,
			renderer->ambient.color_g,
			renderer->ambient.color_b
			);

		// #note: update dir lights
		gl.uniform1ui(
			renderer->deferred_shader->deferred.num_dir_lights_id,
			renderer->num_dir_lights
			);
		gl.bind_buffer(GL_SHADER_STORAGE_BUFFER, renderer->deferred_shader->deferred.dir_lights_ssbo);
		void *ptr = gl.map_buffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
		memcpy(ptr, &renderer->dir_lights[0], sizeof(dir_light) * renderer->num_dir_lights);
		b32 result = gl.unmap_buffer(GL_SHADER_STORAGE_BUFFER);
		if (!result) { printf("dir_light ssbo fail\n"); }
		ptr = 0;

		gl.bind_buffer_base(
			GL_SHADER_STORAGE_BUFFER,
			renderer->deferred_shader->deferred.dir_lights_binding_id,
			renderer->deferred_shader->deferred.dir_lights_ssbo
			);
	
		// #note: update point lights
		gl.uniform1ui(
			renderer->deferred_shader->deferred.num_point_lights_id,
			renderer->num_point_lights
			);
		gl.bind_buffer(GL_SHADER_STORAGE_BUFFER, renderer->deferred_shader->deferred.point_lights_ssbo);
		ptr = gl.map_buffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
		memcpy(ptr, &renderer->point_lights[0], sizeof(point_light) * renderer->num_point_lights);
		result = gl.unmap_buffer(GL_SHADER_STORAGE_BUFFER);
		if (!result) { printf("point_light ssbo fail\n"); }
		ptr = 0;

		gl.bind_buffer_base(
			GL_SHADER_STORAGE_BUFFER,
			renderer->deferred_shader->deferred.point_lights_binding_id,
			renderer->deferred_shader->deferred.point_lights_ssbo
			);

		// #note update spot lights
		gl.uniform1ui(
			renderer->deferred_shader->deferred.num_spot_lights_id,
			renderer->num_spot_lights
			);
		gl.bind_buffer(GL_SHADER_STORAGE_BUFFER, renderer->deferred_shader->deferred.spot_lights_ssbo);
		ptr = gl.map_buffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
		memcpy(ptr, &renderer->spot_lights[0], sizeof(spot_light) * renderer->num_spot_lights);
		result = gl.unmap_buffer(GL_SHADER_STORAGE_BUFFER);
		if (!result) { printf("spot_light ssbo fail\n"); }
		ptr = 0;

		gl.bind_buffer_base(
			GL_SHADER_STORAGE_BUFFER,
			renderer->deferred_shader->deferred.spot_lights_binding_id,
			renderer->deferred_shader->deferred.spot_lights_ssbo
			);

		// #note render the render plane
		gl.bind_vertex_array(renderer->render_plane.vao);
		gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, renderer->render_plane.ibo);
		gl.draw_elements(GL_TRIANGLES, renderer->render_plane.num_indices,
						 GL_UNSIGNED_INT, (void*)0);
		gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		gl.bind_vertex_array(0);
		gl.use_program(0);

		CHECK_GL_ERROR;
	}

	// #note renderer clean up/reset
	memory_arena_clear(&renderer->arena);
	renderer->num_dir_lights = 0;
	renderer->num_point_lights = 0;
	renderer->num_spot_lights = 0;
#endif
}
