/*
  - #todo

  - revert to a simpler lighting system, only ambient and diffuse,
    maybe just not pbr
	-- update -- made a shader to use the non-pbr, and then
	   reimplemented pbr, with some success. On non-flat surfaces
	   there still seems to be a bit of a problem, but this might
	   due to using smaller textures, although it doesn't appear
	   to be the case in the display views. Will have to return to
	   the topic once higher res textures are in place.

  - add a sky-box, with a rudimentary day/night cycle
  - add fog based on day/night cycle
  - create and use new textures for the player character.
  - add a few pbr models, like an obelisk ...
  - attempt using image-based-lighting from the skybox
  - load in the landscape and platform
  - implement orbiting camera, along with the normal first person for
    debugging purposes
  - simple player movement (no physics, just along the x-z plane)
  - animation!

  - start pre-rendering the "backdrop" of planets, and add image based
    lighting.

  - start work on physics with large numbers, orbit visualization,
    acceleration

  - large number of audio underruns. May be due to very poor
    performance, though it should probably be on its own thread...
 */

#include "host.h"
#include <apl_gl.h>

apl_gl gl;

#include "assets.c"
#include "entity.c"
#include "render.c"

// #todo add color functions to enable playing with hue, saturation, and lightness.

void host_init(game_state *state)
{
	state->num_frames = 0;
	state->time_elapsed_since_start = 0.0f;
	state->current_time_elapsed = 0.0f;

	load_assets(&state->assets, &state->renderer);

	add_camera(state, v3(0.0f, 1.1f, 0.0f), to_radians(60.0f));

	// day
 	//add_dir_lamp(state, q4_from_euler_angles(0.0f, 0.0f, 0.0f), v3(1.0f, 1.0f, 1.0f), 100.0f);
	// dusk
	//add_dir_lamp(state, q4_from_euler_angles(0.0f, 1.3f, 0.0f), v3(0.3451f, 0.2549f, 0.2196f), 30.0f);
	// night / indoor
	add_point_lamp(state, v3(0.0f, 1.0f, 0.0f), v3(1.0f, 1.0f, 1.0f), 0.2f);
	/*
	add_spot_lamp(state, v3(0.0f, 2.0f, 0.0f), q4_identity(),
				  v3(0.2f, 0.2f, 0.6f), 0.4f, 0.5f, 5.0f);
	*/
	
	// #note fill up point light buffer to ensure it isn't an indexing issue
	u32 num_lights = 0;
	for (u32 z = 0; z < num_lights; ++z)
	{
		for (u32 x = 0; x < num_lights; ++x)
		{
			/* f32 x_pos = -4.5f + x * 3.0f; */
			/* f32 z_pos = 4.5f - z * 3.0f; */
			f32 x_pos = -3.0f + x * 2.0f;
			f32 z_pos = 3.0f - z * 2.0f;

			add_point_lamp(state, v3(x_pos, 1.0f, z_pos), v3(1.0f, 1.0f, 1.0f), 0.2f);
		}
	}

#if 1
	// #note +x
	add_static_mesh(
			state,
			RENDERMESH_LINES,
			RENDERFLAGS_SHADELESS,
			v3(0.0f, 0.0f, 0.0f),
			v3(10.0f, 10.0f, 10.0f),
			q4_identity(),
			v3(0.851f, 0.275f, 0.231f),
			get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLAIN),
			get_mesh_id(&state->assets, ASSET_MESH_POSXAXIS)
	);
	// #note -x
	add_static_mesh(
			state,
			RENDERMESH_LINES,
			RENDERFLAGS_SHADELESS,
			v3(0.0f, 0.0f, 0.0f),
			v3(10.0f, 10.0f, 10.0f),
			q4_identity(),
			v3(0.627f, 0.090f, 0.051f),
			get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLAIN),
			get_mesh_id(&state->assets, ASSET_MESH_NEGXAXIS)
	);
	// #note +y
	add_static_mesh(
			state,
			RENDERMESH_LINES,
			RENDERFLAGS_SHADELESS,
			v3(0.0f, 0.0f, 0.0f),
			v3(10.0f, 10.0f, 10.0f),
			q4_identity(),
			v3(0.455f, 0.851f, 0.231f),
			get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLAIN),
			get_mesh_id(&state->assets, ASSET_MESH_POSYAXIS)
	);
	// #note -y
	add_static_mesh(
			state,
			RENDERMESH_LINES,
			RENDERFLAGS_SHADELESS,
			v3(0.0f, 0.0f, 0.0f),
			v3(10.0f, 10.0f, 10.0f),
			q4_identity(),
			v3(0.259f, 0.627f, 0.051f),
			get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLAIN),
			get_mesh_id(&state->assets, ASSET_MESH_NEGYAXIS)
	);
	// #note +z
	add_static_mesh(
			state,
			RENDERMESH_LINES,
			RENDERFLAGS_SHADELESS,
			v3(0.0f, 0.0f, 0.0f),
			v3(10.0f, 10.0f, 10.0f),
			q4_identity(),
			v3(0.000f, 0.271f, 0.851f),
			get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLAIN),
			get_mesh_id(&state->assets, ASSET_MESH_POSZAXIS)
	);
	// #note -z
	add_static_mesh(
			state,
			RENDERMESH_LINES,
			RENDERFLAGS_SHADELESS,
			v3(0.0f, 0.0f, 0.0f),
			v3(10.0f, 10.0f, 10.0f),
			q4_identity(),
			v3(0.051f, 0.329f, 0.627f),
			get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLAIN),
			get_mesh_id(&state->assets, ASSET_MESH_NEGZAXIS)
	);
#endif

#if 1
	// #note front cube
	add_static_mesh(
			state,
			RENDERMESH_TRIANGLES,
			0,
			v3(0.0f, 0.0f, -4.0f),
			v3(1.0f, 1.0f, 1.0f),
			q4_identity(),
			v3(1.0f, 1.0f, 1.0f),
			get_texture_id(&state->assets, ASSET_TEXTURE_DIFFUSE_CUBE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_CUBE),
			get_mesh_id(&state->assets, ASSET_MESH_CUBE)
	);
	// #note right cube
	add_static_mesh(
			state,
			RENDERMESH_TRIANGLES,
			0,
			v3(4.0f, 0.0f, 0.0f),
			v3(1.0f, 1.0f, 1.0f),
			q4_identity(),
			v3(1.0f, 1.0f, 1.0f),
			get_texture_id(&state->assets, ASSET_TEXTURE_DIFFUSE_CUBE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_CUBE),
			get_mesh_id(&state->assets, ASSET_MESH_CUBE)
	);
	// #note left sphere
	add_static_mesh(
			state,
			RENDERMESH_TRIANGLES,
			0,
			v3(-4.0f, 0.0f, 0.0f),
			v3(1.0f, 1.0f, 1.0f),
			q4_identity(),
			v3(1.0f, 1.0f, 1.0f),
			get_texture_id(&state->assets, ASSET_TEXTURE_DIFFUSE_CUBE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLANE),
			get_mesh_id(&state->assets, ASSET_MESH_SPHERE)
	);

	// #note back sphere
	add_static_mesh(
		state,
		RENDERMESH_TRIANGLES,
		0,
		v3(0.0f, 0.0f, 4.0f),
		v3(1.0f, 1.0f, 1.0f),
		q4_identity(),
		v3(1.0f, 1.0f, 1.0f),
		get_texture_id(&state->assets, ASSET_TEXTURE_DIFFUSE_CUBE),
		get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLANE),
		get_mesh_id(&state->assets, ASSET_MESH_SPHERE_HP)
		);
#endif

#if 1
	// #note ground plane
	add_static_mesh(
			state,
			RENDERMESH_TRIANGLES,
			0,
			v3(0.0f, -0.5f, 0.0f),
			v3(32.0f, 32.0f, 32.0f),
			q4_identity(),
			v3(1.0f, 1.0f, 1.0f),
			get_texture_id(&state->assets, ASSET_TEXTURE_DIFFUSE_CUBE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLANE),
			get_mesh_id(&state->assets, ASSET_MESH_PLANE)
	);
#endif

#if 1
	// #note character
	add_static_mesh(
		state,
		RENDERMESH_TRIANGLES,
		0,
		v3(0.0f, -0.5f, -1.0f),
		v3(1.0f, 1.0f, 1.0f),
		q4(-1.0f, 0.0f, 0.0f, 1.0f),
		v3(1.0f, 1.0f, 1.0f),
		get_texture_id(&state->assets, ASSET_TEXTURE_DIFFUSE_CHARACTER),
		get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_CHARACTER),
		get_mesh_id(&state->assets, ASSET_MESH_CHARACTER)
		);
#endif

	set_gl_state_3d();
	init_deferred_renderer(
			&state->renderer,
			get_mesh_id(&state->assets, ASSET_RENDER_PLANE_MESH),
			get_glshader(&state->assets, ASSET_SKYBOX_SHADER),
			get_glshader(&state->assets, ASSET_GBUFFER_SHADER),
			get_glshader(&state->assets, ASSET_DEFERRED_SHADER),
			state->sys.client_width,
			state->sys.client_height,
			state->sys.client_width,
			state->sys.client_height
	);

//	render_push_skybox(&state->renderer, state->assets.assets + ASSET_TEXTURE_DIFFUSE_CUBE);
	render_push_skybox(&state->renderer, state->assets.assets + ASSET_TEXTURE_SKYBOX);
	//render_push_skybox(&state->renderer, state->assets.assets + ASSET_CUBEMAP_DEFAULT_SKYBOX);

	printf("host: initialized.\n");

	state->initialized = true;
}

void host_shutdown(game_state *state)
{
	printf("host: shutdown.\n");
}

b32 host_frame(game_memory *memory, game_input *input, apl_gl *passed_gl)
{
	game_state* state = (game_state*)memory->storage;
	gl = *passed_gl;

	if (!state->initialized)
	{
		input->center_cursor = true;
		host_init(state);
	}

	++state->num_frames;
//	printf("frames: %" priu64 "\n", state->num_frames);
	state->time_elapsed_since_start += input->dt_frame;
	state->current_time_elapsed += input->dt_frame;

	if (input->f1.released)
	{
		state->renderer.output_mode = RENDEROUT_COMPLETE;
	}
	if (input->f2.released)
	{
		state->renderer.output_mode = RENDEROUT_ALBEDO;
	}
	if (input->f3.released)
	{
		state->renderer.output_mode = RENDEROUT_NORMAL;
	}
	if (input->f4.released)
	{
		state->renderer.output_mode = RENDEROUT_POSITION;
	}
	if (input->f5.released)
	{
		state->renderer.output_mode = RENDEROUT_METALLIC;
	}
	if (input->f6.released)
	{
		state->renderer.output_mode = RENDEROUT_ROUGHNESS;
	}
	if (input->f7.released)
	{
		state->renderer.output_mode = RENDEROUT_EMISSIVE;
	}

	// #note update
	for (u32 entity_idx = 0; entity_idx < state->num_entities; ++entity_idx)
	{
		entity *current_entity = state->entities + entity_idx;
		switch (current_entity->type)
		{
			case ENTITYTYPE_EMPTY:
			{
				continue;
			} break;
			case ENTITYTYPE_DIR_LAMP:
			{
#if 0
				persist f32 t = 0.0f;
				t += input->dt_frame;
				f32 freq = 0.1f;
				current_entity->transform.rotation = q4_from_euler_angles(0.0f, t * freq * PI_2, 0.0f);
				current_entity->dir_lamp.lamp->transform.rotation =
					current_entity->transform.rotation;
#endif
			} break;
			case ENTITYTYPE_POINT_LAMP:
			{
				persist f32 t = 0.0f;
				f32 hertz = 0.5f;
				t += input->dt_frame;
				//t = 2.6f;
				f32 point_y = 0.5f; //3.0f * cosf32(PI_2 * t * hertz) + 3.1f;
				current_entity->transform.position.y = point_y;
				current_entity->point_lamp.lamp->transform.position =
					current_entity->transform.position;
			} break;
			case ENTITYTYPE_SPOT_LAMP:
			{
				persist f32 t = 0.0f;
				t += input->dt_frame;
				f32 up_hertz = 0.1f;
				f32 point_y = 2.0f * cosf32(PI_2 * t * up_hertz) + 3.0f;
				f32 rot_freq = 0.3f;
				f32 cut_freq = 0.5f;
				current_entity->spot_lamp.outer_cutoff = 0.2f * sinf32(t * cut_freq * PI_2) + 0.7f;
				current_entity->spot_lamp.inner_cutoff = current_entity->spot_lamp.outer_cutoff + 0.05f;
#if 0
				current_entity->transform.position.y = point_y;
				current_entity->transform.rotation = q4_from_euler_angles(0.0f, 0.5f * sinf32(t * rot_freq * PI_2), 0.0f);
#endif
				current_entity->spot_lamp.lamp->transform = current_entity->transform;
			} break;
			case ENTITYTYPE_CAMERA3D:
			{
				f32 move_speed = 3.0f;
				f32 y_sens = 0.10f;
				f32 x_sens = 0.7f * y_sens;

				/* #todo if other errors with quaternion rotation appear then fix them and
				   try using quaternions for the camera rotation again
				*/
				persist f32 pitch_angle = 0.0f;
				persist f32 yaw_angle = 0.0f;
				pitch_angle += input->d_mouse_y * input->dt_frame * x_sens;
				yaw_angle += input->d_mouse_x * input->dt_frame * y_sens;

				pitch_angle = clamp_angle(pitch_angle);
				yaw_angle = clamp_angle(yaw_angle);

				mat4 yrot = m4_rotation(yaw_angle, v3(0.0f, 1.0f, 0.0f));
				mat4 xrot = m4_rotation(pitch_angle, v3(1.0f, 0.0f, 0.0f));
				mat4 rot = m4_mul_m4(xrot, yrot);
				current_entity->transform.rotation = q4_from_m4(rot);

				mat4 cam_r_mat = m4_rotation_quaternion(current_entity->transform.rotation);
				vec3 cam_x_axis = v3(cam_r_mat.a11, cam_r_mat.a12, cam_r_mat.a13);
				vec3 cam_y_axis = v3(cam_r_mat.a21, cam_r_mat.a22, cam_r_mat.a23);
				vec3 cam_z_axis = v3(cam_r_mat.a31, cam_r_mat.a32, cam_r_mat.a33);

				vec3 dx = v3_mul(
						move_speed * input->keyboard.left_horizontal_axis * input->dt_frame,
						cam_x_axis
				);
				f32 y_axis = 0.0f;
				if (!(input->keyboard.right_shoulder.down && input->keyboard.left_shoulder.down))
				{
					if (input->keyboard.right_shoulder.down)
					{
						y_axis = 1.0f;
					}
					else if (input->keyboard.left_shoulder.down)
					{
						y_axis = -1.0f;
					}
				}
				vec3 dy = v3_mul(
						move_speed * y_axis * input->dt_frame,
						cam_y_axis
				);
				vec3 dz = v3_mul(
						-move_speed * input->keyboard.left_vertical_axis * input->dt_frame,
						cam_z_axis
				);

				current_entity->transform.position = v3_add(
						current_entity->transform.position,
						v3_add(dz, v3_add(dx, dy))
				);

				update_camera_entity(current_entity);
			} break;
			case ENTITYTYPE_STATIC_MESH:
			{
			} break;
			default:
			{
				ASSERT(0);
			} break;
		}
	}

	// #note render
	//vec3 acol = v3(0.0f, 0.0f, 0.0f);
	// day
	//vec3 acol = v3(0.1333f, 0.2353f, 0.3608f);
	// dusk 10202b
	//vec3 acol = v3(0.0314f, 0.0627f, 0.0824f);
	// night / indoor
	vec3 acol = v3(0.001f, 0.001f, 0.001f);
	render_push_clear(&state->renderer, acol);
	render_push_ambient_color(&state->renderer, acol);

	for (u32 entity_idx = 0; entity_idx < state->num_entities; ++entity_idx)
	{
		entity *current_entity = state->entities + entity_idx;
		switch (current_entity->type)
		{
			case ENTITYTYPE_EMPTY:
			{
				continue;
			} break;
			case ENTITYTYPE_DIR_LAMP:
			{
				render_push_directional_lamp(
						&state->renderer,
						current_entity->transform.rotation,
						current_entity->dir_lamp.color,
						current_entity->point_lamp.intensity
				);
			} break;
			case ENTITYTYPE_POINT_LAMP:
			{
				render_push_point_lamp(
						&state->renderer,
						current_entity->transform.position,
						current_entity->point_lamp.color,
						current_entity->point_lamp.intensity
				);
			} break;
			case ENTITYTYPE_SPOT_LAMP:
			{
				render_push_spot_lamp(
						&state->renderer,
						current_entity->transform.position,
						current_entity->transform.rotation,
						current_entity->spot_lamp.color,
						current_entity->point_lamp.intensity,
						current_entity->spot_lamp.inner_cutoff,
						current_entity->spot_lamp.outer_cutoff
				);
			} break;
			case ENTITYTYPE_CAMERA3D:
			{
				render_push_active_camera(
						&state->renderer,
						current_entity->camera,
						current_entity->transform.position
				);
			} break;
			case ENTITYTYPE_STATIC_MESH:
			{
				render_push_mesh(
						&state->renderer,
						current_entity->static_mesh.type,
						current_entity->static_mesh.flags,
						current_entity->static_mesh.color,
						current_entity->static_mesh.diffuse_texture_id,
						current_entity->static_mesh.normal_texture_id,
						current_entity->static_mesh.mesh_id,
						current_entity->transform
				);
			} break;
			default:
			{
				ASSERT(0);
			} break;
		}
	}

	deferred_renderer_draw(&state->renderer);

	// #note: End of frame
	if(input->keyboard.back.released)
	{
		host_shutdown(state);
		return false;
	}
	else
	{
		return true;
	}
}
