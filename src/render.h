#ifndef RENDER_H
#define RENDER_H

typedef enum render_output_mode
{
	RENDEROUT_COMPLETE,
	RENDEROUT_ALBEDO,
	RENDEROUT_NORMAL,
	RENDEROUT_POSITION,
	RENDEROUT_METALLIC,
	RENDEROUT_ROUGHNESS,
	RENDEROUT_EMISSIVE
} render_output_mode;

typedef enum render_instruction
{
	RENDERINST_CLEAR,
	RENDERINST_MESH
} render_instruction;

typedef struct ambient_light
{
	f32 color_r;
	f32 color_g;
	f32 color_b;
} ambient_light;

typedef struct dir_light
{
	f32 color_r;
	f32 color_g;
	f32 color_b;
	f32 dir_x;
	f32 dir_y;
	f32 dir_z;
} dir_light;

typedef struct point_light
{
	f32 color_r;
	f32 color_g;
	f32 color_b;
	f32 pos_x;
	f32 pos_y;
	f32 pos_z;
} point_light;

typedef struct spot_light
{
	f32 color_r;
	f32 color_g;
	f32 color_b;
	f32 pos_x;
	f32 pos_y;
	f32 pos_z;
	f32 dir_x;
	f32 dir_y;
	f32 dir_z;
	f32 inner_cutoff;
	f32 outer_cutoff;
} spot_light;

#define RENDERFLAGS_SHADELESS	0x0001
#define RENDERFLAGS_UNSET0		0x0002
#define RENDERFLAGS_UNSET1		0x0004
#define RENDERFLAGS_UNSET2		0x0008
#define RENDERFLAGS_UNSET3		0x0010
#define RENDERFLAGS_UNSET4		0x0020
#define RENDERFLAGS_UNSET5		0x0040
#define RENDERFLAGS_UNSET6		0x0080
#define RENDERFLAGS_UNSET7		0x0100
#define RENDERFLAGS_UNSET8		0x0200
#define RENDERFLAGS_UNSET9		0x0400
#define RENDERFLAGS_UNSET10		0x0800
#define RENDERFLAGS_UNSET11		0x1000
#define RENDERFLAGS_UNSET12		0x2000
#define RENDERFLAGS_UNSET13		0x4000
#define RENDERFLAGS_UNSET14		0x8000

typedef enum render_mesh_type
{
	RENDERMESH_LINES,
	RENDERMESH_TRIANGLES
} render_mesh_type;

typedef struct render_mesh
{
	render_mesh_type type;
	u16 flags;
	vec3 color;
	texture_id diffuse;
	texture_id normal;
	mesh_id mesh_id;
	mat4 model;
} render_mesh;

// #todo implement this for general use
typedef struct memory_arena
{
	void *data;
	size_t size;
	size_t current_size;
} memory_arena;

void memory_arena_init(memory_arena *arena, size_t size)
{
	arena->data = ast_malloc(size);
	arena->current_size = 0;
	arena->size = size;
}

void memory_arena_push(memory_arena *arena, void *data, size_t size)
{
	ASSERT(arena->current_size + size < arena->size);
	memcpy((u8 *)arena->data + arena->current_size, data, size);
	arena->current_size += size;
}

void memory_arena_clear(memory_arena *arena)
{
	arena->current_size = 0;
}

// #todo once a suitable memory arena and a better buffer system replace arrays, updating ssbo sizes
// will need to be investigated
#define MAX_POINT_LIGHTS 16
#define MAX_SPOT_LIGHTS 8
#define MAX_DIR_LIGHTS 4
#define MAX_ENTITIES 64
typedef struct deferred_renderer
{
	memory_arena arena;

	mesh_id render_plane;
	
	ambient_light ambient;
	u32 num_dir_lights;
	dir_light dir_lights[MAX_DIR_LIGHTS];
	u32 num_point_lights;
	point_light point_lights[MAX_POINT_LIGHTS];
	u32 num_spot_lights;
	spot_light spot_lights[MAX_SPOT_LIGHTS];

	mat4 projection;
	mat4 view;
	vec3 cam_pos;

	b32 render_3d_scene;

	glshader *skybox_shader;
	glshader *gbuffer_shader;
	glshader *deferred_shader;

	u32 gb_framebuffer;
	u32 gb_albedo_metallic_texture;
	u32 gb_normal_roughness_texture;
	u32 gb_position_texture;

	u32 internal_render_width;
	u32 internal_render_height;
	u32 viewport_width;
	u32 viewport_height;

	game_asset *skybox;
	render_mesh skybox_mesh;

	render_output_mode output_mode;
} deferred_renderer;

#endif// !RENDER_H
