const char* default_vertex_shader =
	"#version 330\nlayout(location=0) in vec3 position;\nuniform mat4 mvp;\n"
	"void main()\n{\ngl_Position = mvp * vec4(position, 1.0);\n}\n";

const char* default_fragment_shader =
	"#version 330\nout vec4 color;\nvoid main()\n{\n"
	"color = vec4(1.0, 0.0, 1.0, 1.0);\n}\n";

#define CONSUME(ptr, type) ((type *)consume(&ptr, sizeof(type)))
void *consume(u8 **ptr, u64 num_bytes)
{
	void *result = *ptr;
	*ptr += num_bytes;
	return result;
}

void gen_texture_asset(game_assets *assets, u8 *asset_data)
{
	asset_specifier spec = *CONSUME(asset_data, asset_specifier);
	game_asset *asset = assets->assets + spec;
	asset->type = ASSETTYPE_TEXTURE;
	asset->texture.data.width = *CONSUME(asset_data, u32);
	asset->texture.data.height = *CONSUME(asset_data, u32);
	asset->texture.data.pixels = asset_data;

	gl.gen_textures(1, &asset->texture.texture_id);
	ASSERT(asset->texture.texture_id != 0);
	gl.bind_texture(GL_TEXTURE_2D, asset->texture.texture_id);
	gl.tex_image_2d(
			GL_TEXTURE_2D,
			0,
			GL_RGBA,
			asset->texture.data.width,
			asset->texture.data.height,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			asset->texture.data.pixels
	);
	// #todo the assets should determine filtering, check other parameters
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	// #todo clamping the textures is causing expected artifacts in
	// the mannequin character, should probably reset the texture map
//	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	gl.tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	gl.generate_mipmap(GL_TEXTURE_2D);

	gl.bind_texture(GL_TEXTURE_2D, 0);

	CHECK_GL_ERROR;
}

u8 white_texture[4 * 2 * 2] =
{
	0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff
};
#if 0
void gen_default_skybox(game_assets *assets)
{
	game_asset *asset = assets->assets + ASSET_CUBEMAP_DEFAULT_SKYBOX;
	asset->type = ASSETTYPE_CUBE_MAP;
	asset->cubemap.width = 2;
	asset->cubemap.height = 2;
	asset->cubemap.side_pixels = white_texture;
	asset->cubemap.up_pixels = white_texture;
	asset->cubemap.down_pixels = white_texture;

	u32 width = asset->cubemap.width;
	u32 height = asset->cubemap.height;
	
	gl.gen_textures(1, &asset->cubemap.cubemap_id);
	gl.bind_texture(GL_TEXTURE_CUBE_MAP, asset->cubemap.cubemap_id);
	
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.side_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.side_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.up_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.down_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.side_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.side_pixels);
	
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	gl.bind_texture(GL_TEXTURE_CUBE_MAP, 0);

	CHECK_GL_ERROR;
}

void gen_cubemap_asset(game_assets *assets, u8 *asset_data)
{
	// #todo currently the cubemaps will have a repeated texture for
	// all sides, if it is desirable to add unique elements such as
	// clouds, or to not have all the stars in every horizontal
	// direction be the same, then this will need to be
	// restructured. It may be favorable to use a different method if
	// that isn't the case, to halve the texture memory use.
	asset_specifier spec = *CONSUME(asset_data, asset_specifier);
	game_asset *asset = assets->assets + spec;
	asset->type = ASSETTYPE_CUBE_MAP;
	asset->cubemap.width = *CONSUME(asset_data, u32);
	asset->cubemap.height = *CONSUME(asset_data, u32);
	u64 bytes_per_texture = 4 * asset->cubemap.width * asset->cubemap.height;
	asset->cubemap.side_pixels = consume(&asset_data, bytes_per_texture);
	asset->cubemap.up_pixels = consume(&asset_data, bytes_per_texture);
	asset->cubemap.down_pixels = asset_data;

	u32 width = asset->cubemap.width;
	u32 height = asset->cubemap.height;
	
	gl.gen_textures(1, &asset->cubemap.cubemap_id);
	gl.bind_texture(GL_TEXTURE_CUBE_MAP, asset->cubemap.cubemap_id);
	
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.side_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.side_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.up_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.down_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.side_pixels);
	gl.tex_image_2d(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, asset->cubemap.side_pixels);
	
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	gl.tex_parameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	gl.bind_texture(GL_TEXTURE_CUBE_MAP, 0);

	CHECK_GL_ERROR;
}
#endif
void gen_mesh_asset(game_assets *assets, u8 *asset_data)
{
	asset_specifier spec = *CONSUME(asset_data, asset_specifier);
	game_asset *asset = assets->assets + spec;
	asset->type = ASSETTYPE_MESH;
	
	asset->mesh.data.num_vertices = *CONSUME(asset_data, u32);
	asset->mesh.data.num_indices = *CONSUME(asset_data, u32);
	asset->mesh.data.positions = (f32 *)asset_data;
	asset_data += 3 * asset->mesh.data.num_vertices * sizeof(f32);
	asset->mesh.data.normals = (f32 *)asset_data;
	asset_data += 3 * asset->mesh.data.num_vertices * sizeof(f32);
	asset->mesh.data.tangents = (f32 *)asset_data;
	asset_data += 3 * asset->mesh.data.num_vertices * sizeof(f32);
	asset->mesh.data.uvs = (f32 *)asset_data;
	asset_data += 2 * asset->mesh.data.num_vertices * sizeof(f32);
	asset->mesh.data.indices = (u32 *)asset_data;

	gl.gen_vertex_arrays(1, &asset->mesh.vao);
	gl.bind_vertex_array(asset->mesh.vao);

	// #note create position buffer object
	gl.gen_buffers(1, &asset->mesh.vbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, asset->mesh.vbo);
	gl.buffer_data(
			GL_ARRAY_BUFFER,
			3 * sizeof(f32) * asset->mesh.data.num_vertices,
			asset->mesh.data.positions,
			GL_STATIC_DRAW
	);
	gl.enable_vertex_attrib_array(0);
	gl.vertex_attrib_pointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);

	// #note create uv buffer object
	gl.gen_buffers(1, &asset->mesh.uvbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, asset->mesh.uvbo);
	gl.buffer_data(
			GL_ARRAY_BUFFER,
			2 * sizeof(f32) * asset->mesh.data.num_vertices,
			asset->mesh.data.uvs,
			GL_STATIC_DRAW
	);
	gl.vertex_attrib_pointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(1);

	// #note create normal buffer object
	gl.gen_buffers(1, &asset->mesh.nbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, asset->mesh.nbo);
	gl.buffer_data(
			GL_ARRAY_BUFFER,
			3 * sizeof(f32) *  asset->mesh.data.num_vertices,
			asset->mesh.data.normals,
			GL_STATIC_DRAW
	);
	gl.vertex_attrib_pointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(2);

	// #note create tangent buffer object
	gl.gen_buffers(1, &asset->mesh.tbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, asset->mesh.tbo);
	gl.buffer_data(
			GL_ARRAY_BUFFER,
			3 * sizeof(f32) * asset->mesh.data.num_vertices,
			asset->mesh.data.tangents,
			GL_STATIC_DRAW
	);
	gl.vertex_attrib_pointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(3);

	gl.bind_vertex_array(0);
	gl.bind_buffer(GL_ARRAY_BUFFER, 0);

	// #note create index buffer object
	gl.gen_buffers(1, &asset->mesh.ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, asset->mesh.ibo);
	gl.buffer_data(
			GL_ELEMENT_ARRAY_BUFFER,
			sizeof(u32) * asset->mesh.data.num_indices,
			asset->mesh.data.indices,
			GL_STATIC_DRAW
	);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	CHECK_GL_ERROR;
}

void compile_shader(u32 shader)
{
	gl.compile_shader(shader);

	s32 result, log_len;
	gl.get_shaderiv(shader, GL_COMPILE_STATUS, &result);
	gl.get_shaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);
	if (log_len > 0)
	{
#if 0
		ASSERT(log_len < 2048);
		char buffer[2048];
#else
		char *buffer = ast_malloc(log_len);
#endif
		gl.get_shader_info_log(shader, log_len, 0, buffer);
		ast_print_color(CCOLOR_RED, 1);
		printf("fatal error: ");
		ast_print_color(CCOLOR_RESET, 0);
		printf("shader compilation failed:\n");
		printf("%s\n", buffer);
		DEBUG_BREAK;
		exit(1);
	}
}

void gen_shader_asset(
		game_assets *assets,
		u8 *asset_data,
		deferred_renderer *renderer
	)
{
	asset_specifier spec = *CONSUME(asset_data, asset_specifier);
	game_asset *asset = assets->assets + spec;
	asset->type = ASSETTYPE_SHADER;
	u64 offset = *CONSUME(asset_data, u64);
	asset->shader.vertex_source = (char *)asset_data;
	asset_data += offset;
	asset->shader.fragment_source = (char *)asset_data;

	u32 vs = gl.create_shader(GL_VERTEX_SHADER);
	u32 fs = gl.create_shader(GL_FRAGMENT_SHADER);

	gl.shader_source(vs, 1, (const char **)&asset->shader.vertex_source, 0);
	compile_shader(vs);
	gl.shader_source(fs, 1, (const char **)&asset->shader.fragment_source, 0);
	compile_shader(fs);

	asset->shader.program = gl.create_program();
	gl.attach_shader(asset->shader.program, vs);
	gl.attach_shader(asset->shader.program, fs);
	gl.link_program(asset->shader.program);

	s32 result, log_len;
	gl.get_programiv(asset->shader.program, GL_LINK_STATUS, &result);
	gl.get_programiv(asset->shader.program, GL_INFO_LOG_LENGTH, &log_len);
	if (log_len > 0)
	{
#if 1
		ASSERT(log_len < 2048);
		char buffer[2048];
#else
		char *buffer = ast_malloc(log_len);
#endif
		gl.get_shader_info_log(asset->shader.program, log_len, 0, buffer);
		ast_print_color(CCOLOR_RED, 1);
		printf("fatal error: ");
		ast_print_color(CCOLOR_RESET, 0);
		printf("program linking failed:\n");
		printf("%s\n", buffer);
		DEBUG_BREAK;
		exit(1);
	}

	gl.detach_shader(asset->shader.program, vs);
	gl.detach_shader(asset->shader.program, fs);

	gl.delete_shader(vs);
	gl.delete_shader(fs);

	CHECK_GL_ERROR;

	// #todo set appropriate shader uniform ids
	switch (spec)
	{
	    case ASSET_SKYBOX_SHADER:
		{
			asset->shader.skybox.projection_id = gl.get_uniform_location(
				asset->shader.program,
				"proj");
			ASSERT(asset->shader.skybox.projection_id != GL_INVALID_INDEX);
			asset->shader.skybox.view_id = gl.get_uniform_location(
				asset->shader.program,
				"view");
			ASSERT(asset->shader.skybox.view_id != GL_INVALID_INDEX);
			asset->shader.skybox.albedo_sampler_id = gl.get_uniform_location(
				asset->shader.program,
				"albedo_sampler");
			ASSERT(asset->shader.skybox.albedo_sampler_id != GL_INVALID_INDEX);
			asset->shader.skybox.color_id = gl.get_uniform_location(
				asset->shader.program,
				"color");
			ASSERT(asset->shader.skybox.color_id != GL_INVALID_INDEX);
		} break;
		case ASSET_GBUFFER_SHADER:
		{
			asset->shader.gbuffer.projection_id = gl.get_uniform_location(
					asset->shader.program,
					"proj"
			);
			ASSERT(asset->shader.gbuffer.projection_id != GL_INVALID_INDEX);
			asset->shader.gbuffer.view_id = gl.get_uniform_location(
					asset->shader.program,
					"view"
			);
			ASSERT(asset->shader.gbuffer.view_id != GL_INVALID_INDEX);
			asset->shader.gbuffer.model_id = gl.get_uniform_location(
					asset->shader.program,
					"model"
			);
			ASSERT(asset->shader.gbuffer.model_id != GL_INVALID_INDEX);
			asset->shader.gbuffer.albedo_metallic_sampler_id = gl.get_uniform_location(
					asset->shader.program,
					"albedo_metallic_sampler"
			);
			ASSERT(asset->shader.gbuffer.albedo_metallic_sampler_id != GL_INVALID_INDEX);
			asset->shader.gbuffer.normal_roughness_sampler_id = gl.get_uniform_location(
					asset->shader.program,
					"normal_roughness_sampler"
			);
			ASSERT(asset->shader.gbuffer.normal_roughness_sampler_id != GL_INVALID_INDEX);
			asset->shader.gbuffer.object_color_id = gl.get_uniform_location(
					asset->shader.program,
					"object_color"
			);
			ASSERT(asset->shader.gbuffer.object_color_id != GL_INVALID_INDEX);
			asset->shader.gbuffer.emissive_id = gl.get_uniform_location(
					asset->shader.program,
					"emissive"
			);
			ASSERT(asset->shader.gbuffer.emissive_id != GL_INVALID_INDEX);
		} break;
		case ASSET_DEFERRED_SHADER:
		{
			asset->shader.deferred.albedo_metallic_sampler_id = gl.get_uniform_location(
					asset->shader.program,
					"albedo_metallic_sampler_id"
			);
			ASSERT(asset->shader.deferred.normal_roughness_sampler_id != GL_INVALID_INDEX);
			asset->shader.deferred.normal_roughness_sampler_id = gl.get_uniform_location(
					asset->shader.program,
					"normal_roughness_sampler"
			);
			ASSERT(asset->shader.deferred.normal_roughness_sampler_id != GL_INVALID_INDEX);
			asset->shader.deferred.pos_sampler_id = gl.get_uniform_location(
					asset->shader.program,
					"pos_sampler"
			);
			ASSERT(asset->shader.deferred.pos_sampler_id != GL_INVALID_INDEX);

			asset->shader.deferred.camera_pos_id = gl.get_uniform_location(
					asset->shader.program,
					"cam_pos"
			);
			ASSERT(asset->shader.deferred.camera_pos_id != GL_INVALID_INDEX);

			asset->shader.deferred.ambient_light_id = gl.get_uniform_location(
					asset->shader.program,
					"ambient_light_color"
			);
			ASSERT(asset->shader.deferred.ambient_light_id != GL_INVALID_INDEX);

			asset->shader.deferred.dir_lights_block_id = gl.get_program_resource_index(
				asset->shader.program,
				GL_SHADER_STORAGE_BLOCK,
				"dir_lights_data"
				);
			ASSERT(
				asset->shader.deferred.dir_lights_block_id != GL_INVALID_INDEX &&
				asset->shader.deferred.dir_lights_block_id != GL_INVALID_ENUM
				);

			asset->shader.deferred.num_dir_lights_id = gl.get_uniform_location(
				asset->shader.program,
				"num_dir_lights"
				);
			ASSERT(asset->shader.deferred.num_dir_lights_id != GL_INVALID_INDEX);
			
			asset->shader.deferred.point_lights_block_id = gl.get_program_resource_index(
					asset->shader.program,
					GL_SHADER_STORAGE_BLOCK,
					"point_lights_data"
			);

			ASSERT(
					asset->shader.deferred.point_lights_block_id != GL_INVALID_INDEX &&
					asset->shader.deferred.point_lights_block_id != GL_INVALID_ENUM
			);

			asset->shader.deferred.num_point_lights_id = gl.get_uniform_location(
					asset->shader.program,
					"num_point_lights"
			);
			ASSERT(asset->shader.deferred.num_point_lights_id != GL_INVALID_INDEX);

			asset->shader.deferred.spot_lights_block_id = gl.get_program_resource_index(
				asset->shader.program,
				GL_SHADER_STORAGE_BLOCK,
				"spot_lights_data"
				);
			ASSERT(
				asset->shader.deferred.spot_lights_block_id != GL_INVALID_INDEX &&
				asset->shader.deferred.spot_lights_block_id != GL_INVALID_ENUM
				);

			asset->shader.deferred.num_spot_lights_id = gl.get_uniform_location(
				asset->shader.program,
				"num_spot_lights"
				);
			ASSERT(asset->shader.deferred.num_spot_lights_id != GL_INVALID_INDEX);
			
			asset->shader.deferred.output_id = gl.get_uniform_location(
					asset->shader.program,
					"output_mode"
			);
			ASSERT(asset->shader.deferred.output_id != GL_INVALID_INDEX);

			gl.gen_buffers(1, &asset->shader.deferred.dir_lights_ssbo);
			gl.bind_buffer(GL_SHADER_STORAGE_BUFFER, asset->shader.deferred.dir_lights_ssbo);
			gl.buffer_data(
				GL_SHADER_STORAGE_BUFFER,
				sizeof(dir_light) * ARRAY_COUNT(renderer->dir_lights),
				&renderer->dir_lights[0],
				GL_DYNAMIC_COPY
				);

			asset->shader.deferred.dir_lights_binding_id = 2;
			gl.shader_storage_block_binding(
				asset->shader.program,
				asset->shader.deferred.dir_lights_block_id,
				asset->shader.deferred.dir_lights_binding_id
				);
			
			gl.gen_buffers(1, &asset->shader.deferred.point_lights_ssbo);
			gl.bind_buffer(GL_SHADER_STORAGE_BUFFER, asset->shader.deferred.point_lights_ssbo);
			gl.buffer_data(
					GL_SHADER_STORAGE_BUFFER,
					sizeof(point_light) * ARRAY_COUNT(renderer->point_lights),
					&renderer->point_lights[0],
					GL_DYNAMIC_COPY
			);

			asset->shader.deferred.point_lights_binding_id = 3;
			gl.shader_storage_block_binding(
					asset->shader.program,
					asset->shader.deferred.point_lights_block_id,
					asset->shader.deferred.point_lights_binding_id
			);

			gl.gen_buffers(1, &asset->shader.deferred.spot_lights_ssbo);
			gl.bind_buffer(GL_SHADER_STORAGE_BUFFER, asset->shader.deferred.spot_lights_ssbo);
			gl.buffer_data(
				GL_SHADER_STORAGE_BUFFER,
				sizeof(spot_light) * ARRAY_COUNT(renderer->spot_lights),
				&renderer->spot_lights[0],
				GL_DYNAMIC_COPY
				);

			asset->shader.deferred.spot_lights_binding_id = 4;
			gl.shader_storage_block_binding(
				asset->shader.program,
				asset->shader.deferred.spot_lights_block_id,
				asset->shader.deferred.spot_lights_binding_id
				);

			gl.bind_buffer(GL_SHADER_STORAGE_BUFFER, 0);
		} break;
		default:
		{
			ASSERT(0);
		} break;
	}
	CHECK_GL_ERROR;
}

void gen_lamps(game_assets *assets)
{
	#if 0
	f32 dlamp_pos_data[] =
	{
		1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		-1.0f, 0.0f, 0.0f
	};

	f32 dlamp_uv_data[] =
	{
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f
	};

	f32 dlamp_index_data[] =
	{
		0, 1,
		2, 3
	};
	#else
	f32 dlamp_pos_data[] =
	{
		0.0f, 0.0f, 0.0f,
		0.2f, 0.0f, 0.0f,
		0.14f, 0.0f, -0.14f,
		0.0f, 0.0f, -0.2f,
		-0.14f, 0.0f, -0.14f,
		-0.2f, 0.0f, 0.0f,
		-0.14f, 0.0f, 0.14f,
		0.0f, 0.0f, 0.2f,
		0.14f, 0.0f, 0.14f,
		0.2f, -1.0f, 0.0f,
		0.14f, -1.0f, -0.14f,
		0.0f, -1.0f, -0.2f,
		-0.14f, -1.0f, -0.14f,
		-0.2f, -1.0f, 0.0f,
		-0.14f, -1.0f, 0.14f,
		0.0f, -1.0f, 0.2f,
		0.14f, -1.0f, 0.14f
	};

	f32 dlamp_uv_data[] =
	{
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f
	};

	f32 dlamp_index_data[] =
	{
		0, 1,
		0, 3,
		0, 5,
		0, 7,

		1, 9,
		2, 10,
		3, 11,
		4, 12,
		5, 13,
		6, 14,
		7, 15,
		8, 16,

		1, 2,
		2, 3,
		3, 4,
		4, 5,
		5, 6,
		6, 7,
		7, 8,
		8, 1
	};
	#endif

	// #note general settings
	u32 num_vertices = ARRAY_COUNT(dlamp_pos_data) / 3;
	size_t n = sizeof(f32) * ARRAY_COUNT(dlamp_pos_data);
	f32 *positions = ast_malloc(n);
	memcpy(positions, dlamp_pos_data, n);
	n = sizeof(f32) * ARRAY_COUNT(dlamp_uv_data);
	f32 *uvs = ast_malloc(n);
	memcpy(uvs, dlamp_uv_data, n);

	u32 vao, vbo, uvbo, nbo, tbo;

	gl.gen_vertex_arrays(1, &vao);
	gl.bind_vertex_array(vao);

	gl.gen_buffers(1, &vbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, vbo);
	gl.buffer_data(GL_ARRAY_BUFFER, 3 * sizeof(f32) * num_vertices, positions, GL_STATIC_DRAW);
	gl.vertex_attrib_pointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(0);

	gl.gen_buffers(1, &uvbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, uvbo);
	gl.buffer_data(GL_ARRAY_BUFFER, 2 * sizeof(f32) * num_vertices, uvs, GL_STATIC_DRAW);
	gl.vertex_attrib_pointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(1);

	gl.gen_buffers(1, &nbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, nbo);
	gl.buffer_data(GL_ARRAY_BUFFER, 3 * sizeof(f32) * num_vertices, positions, GL_STATIC_DRAW);
	gl.vertex_attrib_pointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(2);

	gl.gen_buffers(1, &tbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, tbo);
	gl.buffer_data(GL_ARRAY_BUFFER, 3 * sizeof(f32) * num_vertices, positions, GL_STATIC_DRAW);
	gl.vertex_attrib_pointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(3);

	gl.bind_vertex_array(0);
	gl.bind_buffer(GL_ARRAY_BUFFER, 0);

	game_asset *asset = &assets->assets[ASSET_MESH_DIR_LAMP];
	asset->type = ASSETTYPE_MESH;
	asset->mesh.data = (mesh_data){ 0 };

	asset->mesh.data.num_vertices = ARRAY_COUNT(dlamp_pos_data) / 3;
	asset->mesh.data.num_indices = ARRAY_COUNT(dlamp_index_data);
	asset->mesh.vao = vao;
	asset->mesh.vbo = vbo;
	asset->mesh.uvbo = uvbo;
	asset->mesh.nbo = nbo;
	asset->mesh.tbo = tbo;

	asset->mesh.data.positions = positions;
	asset->mesh.data.uvs = uvs;

	n = sizeof(u32) * ARRAY_COUNT(dlamp_index_data);
	asset->mesh.data.indices = ast_malloc(n);
	#if 1
	for (u32 i = 0; i < asset->mesh.data.num_indices; ++i)
	{
		*(asset->mesh.data.indices + i) = dlamp_index_data[i];
	}
	#else
	// #todo there is a problem with the memcpy here, the first value
	// is correct, but the rest is garbage
	memcpy(
			asset->mesh.data.indices,
			dlamp_index_data,
			n
	);
	#endif

	gl.gen_buffers(1, &asset->mesh.ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, asset->mesh.ibo);
	gl.buffer_data(
			GL_ELEMENT_ARRAY_BUFFER,
			sizeof(u32) * asset->mesh.data.num_indices,
			asset->mesh.data.indices,
			GL_STATIC_DRAW
	);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	CHECK_GL_ERROR;
}

void gen_axes(game_assets *assets)
{
	f32 axes_pos_data[] =
	{
		 0.0f,  0.0f,  0.0f, // #note origin
		 1.0f,  0.0f,  0.0f, // #note +x
		-1.0f,  0.0f,  0.0f, // #note -x
		 0.0f,  1.0f,  0.0f, // #note +y
		 0.0f, -1.0f,  0.0f, // #note -y
		 0.0f,  0.0f,  1.0f, // #note +z
		 0.0f,  0.0f, -1.0f  // #note -z
	};

	f32 axes_uv_data[] =
	{
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f
	};

	u32 pos_x_axis_index_data[] =
	{
		0, 1
	};

	u32 neg_x_axis_index_data[] =
	{
		0, 2
	};

	u32 pos_y_axis_index_data[] =
	{
		0, 3
	};

	u32 neg_y_axis_index_data[] =
	{
		0, 4
	};

	u32 pos_z_axis_index_data[] =
	{
		0, 5
	};

	u32 neg_z_axis_index_data[] =
	{
		0, 6
	};

	// #note general settings
	u32 num_vertices = ARRAY_COUNT(axes_pos_data);
	f32 *positions = ast_malloc(sizeof(f32) * ARRAY_COUNT(axes_pos_data));
	memcpy(positions, axes_pos_data, sizeof(f32) * ARRAY_COUNT(axes_pos_data));
	f32 *uvs = ast_malloc(sizeof(f32) * ARRAY_COUNT(axes_uv_data));
	memcpy(uvs, axes_uv_data, sizeof(f32) * ARRAY_COUNT(axes_uv_data));

	u32 vao, vbo, uvbo, nbo, tbo;

	gl.gen_vertex_arrays(1, &vao);
	gl.bind_vertex_array(vao);

	gl.gen_buffers(1, &vbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, vbo);
	gl.buffer_data(GL_ARRAY_BUFFER, 3 * sizeof(f32) * num_vertices, positions, GL_STATIC_DRAW);
	gl.enable_vertex_attrib_array(0);
	gl.vertex_attrib_pointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);

	gl.gen_buffers(1, &uvbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, uvbo);
	gl.buffer_data(GL_ARRAY_BUFFER, 2 * sizeof(f32) * num_vertices, uvs, GL_STATIC_DRAW);

	gl.gen_buffers(1, &nbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, nbo);
	gl.buffer_data(GL_ARRAY_BUFFER, 3 * sizeof(f32) * num_vertices, positions, GL_STATIC_DRAW);
	gl.vertex_attrib_pointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(2);

	gl.gen_buffers(1, &tbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, tbo);
	gl.buffer_data(GL_ARRAY_BUFFER, 3 * sizeof(f32) * num_vertices, positions, GL_STATIC_DRAW);
	gl.vertex_attrib_pointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(3);

	gl.bind_vertex_array(0);
	gl.bind_buffer(GL_ARRAY_BUFFER, 0);


	// #note +x axis
	game_asset *asset = &assets->assets[ASSET_MESH_POSXAXIS];
	asset->type = ASSETTYPE_MESH;
	asset->mesh.data = (mesh_data){ 0 };

	asset->mesh.data.normals = 0;
	asset->mesh.data.tangents = 0;
	asset->mesh.data.num_vertices = ARRAY_COUNT(axes_pos_data);
	asset->mesh.data.num_indices = ARRAY_COUNT(pos_x_axis_index_data);
	asset->mesh.vao = vao;
	asset->mesh.vbo = vbo;
	asset->mesh.uvbo = uvbo;
	asset->mesh.nbo = nbo;
	asset->mesh.tbo = tbo;

	asset->mesh.data.positions = positions;
	asset->mesh.data.uvs = uvs;

	asset->mesh.data.indices = ast_malloc(sizeof(u32) * ARRAY_COUNT(pos_x_axis_index_data));
	memcpy(
			asset->mesh.data.indices,
			pos_x_axis_index_data,
			sizeof(f32) * ARRAY_COUNT(pos_x_axis_index_data)
	);

	gl.gen_buffers(1, &asset->mesh.ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, asset->mesh.ibo);
	gl.buffer_data(
			GL_ELEMENT_ARRAY_BUFFER,
			sizeof(u32) * asset->mesh.data.num_indices,
			asset->mesh.data.indices,
			GL_STATIC_DRAW
	);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// #note -x axis
	asset = &assets->assets[ASSET_MESH_NEGXAXIS];
	asset->type = ASSETTYPE_MESH;
	asset->mesh.data = (mesh_data){ 0 };

	asset->mesh.data.normals = 0;
	asset->mesh.data.tangents = 0;
	asset->mesh.data.num_vertices = ARRAY_COUNT(axes_pos_data);
	asset->mesh.data.num_indices = ARRAY_COUNT(neg_x_axis_index_data);
	asset->mesh.vao = vao;
	asset->mesh.vbo = vbo;
	asset->mesh.uvbo = uvbo;
	asset->mesh.nbo = nbo;
	asset->mesh.tbo = tbo;

	asset->mesh.data.positions = positions;
	asset->mesh.data.uvs = uvs;

	asset->mesh.data.indices = ast_malloc(sizeof(u32) * ARRAY_COUNT(neg_x_axis_index_data));
	memcpy(
			asset->mesh.data.indices,
			neg_x_axis_index_data,
			sizeof(f32) * ARRAY_COUNT(neg_x_axis_index_data)
	);

	gl.gen_buffers(1, &asset->mesh.ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, asset->mesh.ibo);
	gl.buffer_data(
			GL_ELEMENT_ARRAY_BUFFER,
			sizeof(u32) * asset->mesh.data.num_indices,
			asset->mesh.data.indices,
			GL_STATIC_DRAW
	);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// #note +y axis
	asset = &assets->assets[ASSET_MESH_POSYAXIS];
	asset->type = ASSETTYPE_MESH;
	asset->mesh.data = (mesh_data){ 0 };

	asset->mesh.data.normals = 0;
	asset->mesh.data.tangents = 0;
	asset->mesh.data.num_vertices = ARRAY_COUNT(axes_pos_data);
	asset->mesh.data.num_indices = ARRAY_COUNT(pos_y_axis_index_data);
	asset->mesh.vao = vao;
	asset->mesh.vbo = vbo;
	asset->mesh.uvbo = uvbo;
	asset->mesh.nbo = nbo;
	asset->mesh.tbo = tbo;

	asset->mesh.data.positions = positions;
	asset->mesh.data.uvs = uvs;

	asset->mesh.data.indices = ast_malloc(sizeof(u32) * ARRAY_COUNT(pos_y_axis_index_data));
	memcpy(
			asset->mesh.data.indices,
			pos_y_axis_index_data,
			sizeof(f32) * ARRAY_COUNT(pos_y_axis_index_data)
	);

	gl.gen_buffers(1, &asset->mesh.ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, asset->mesh.ibo);
	gl.buffer_data(
			GL_ELEMENT_ARRAY_BUFFER,
			sizeof(u32) * asset->mesh.data.num_indices,
			asset->mesh.data.indices,
			GL_STATIC_DRAW
	);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// #note -y axis
	asset = &assets->assets[ASSET_MESH_NEGYAXIS];
	asset->type = ASSETTYPE_MESH;
	asset->mesh.data = (mesh_data){ 0 };

	asset->mesh.data.normals = 0;
	asset->mesh.data.tangents = 0;
	asset->mesh.data.num_vertices = ARRAY_COUNT(axes_pos_data);
	asset->mesh.data.num_indices = ARRAY_COUNT(neg_y_axis_index_data);
	asset->mesh.vao = vao;
	asset->mesh.vbo = vbo;
	asset->mesh.uvbo = uvbo;
	asset->mesh.nbo = nbo;
	asset->mesh.tbo = tbo;

	asset->mesh.data.positions = positions;
	asset->mesh.data.uvs = uvs;

	asset->mesh.data.indices = ast_malloc(sizeof(u32) * ARRAY_COUNT(neg_y_axis_index_data));
	memcpy(
			asset->mesh.data.indices,
			neg_y_axis_index_data,
			sizeof(f32) * ARRAY_COUNT(neg_y_axis_index_data)
	);

	gl.gen_buffers(1, &asset->mesh.ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, asset->mesh.ibo);
	gl.buffer_data(
			GL_ELEMENT_ARRAY_BUFFER,
			sizeof(u32) * asset->mesh.data.num_indices,
			asset->mesh.data.indices,
			GL_STATIC_DRAW
	);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// #note +z axis
	asset = &assets->assets[ASSET_MESH_POSZAXIS];
	asset->type = ASSETTYPE_MESH;
	asset->mesh.data = (mesh_data){ 0 };

	asset->mesh.data.normals = 0;
	asset->mesh.data.tangents = 0;
	asset->mesh.data.num_vertices = ARRAY_COUNT(axes_pos_data);
	asset->mesh.data.num_indices = ARRAY_COUNT(pos_z_axis_index_data);
	asset->mesh.vao = vao;
	asset->mesh.vbo = vbo;
	asset->mesh.uvbo = uvbo;
	asset->mesh.nbo = nbo;
	asset->mesh.tbo = tbo;

	asset->mesh.data.positions = positions;
	asset->mesh.data.uvs = uvs;

	asset->mesh.data.indices = ast_malloc(sizeof(u32) * ARRAY_COUNT(pos_z_axis_index_data));
	memcpy(
			asset->mesh.data.indices,
			pos_z_axis_index_data,
			sizeof(f32) * ARRAY_COUNT(pos_z_axis_index_data)
	);

	gl.gen_buffers(1, &asset->mesh.ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, asset->mesh.ibo);
	gl.buffer_data(
			GL_ELEMENT_ARRAY_BUFFER,
			sizeof(u32) * asset->mesh.data.num_indices,
			asset->mesh.data.indices,
			GL_STATIC_DRAW
	);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// #note -z axis
	asset = &assets->assets[ASSET_MESH_NEGZAXIS];
	asset->type = ASSETTYPE_MESH;
	asset->mesh.data = (mesh_data){ 0 };

	asset->mesh.data.normals = 0;
	asset->mesh.data.tangents = 0;
	asset->mesh.data.num_vertices = ARRAY_COUNT(axes_pos_data);
	asset->mesh.data.num_indices = ARRAY_COUNT(neg_z_axis_index_data);
	asset->mesh.vao = vao;
	asset->mesh.vbo = vbo;
	asset->mesh.uvbo = uvbo;
	asset->mesh.nbo = nbo;
	asset->mesh.tbo = tbo;

	asset->mesh.data.positions = positions;
	asset->mesh.data.uvs = uvs;

	asset->mesh.data.indices = ast_malloc(sizeof(u32) * ARRAY_COUNT(neg_z_axis_index_data));
	memcpy(
			asset->mesh.data.indices,
			neg_z_axis_index_data,
			sizeof(f32) * ARRAY_COUNT(neg_z_axis_index_data)
	);

	gl.gen_buffers(1, &asset->mesh.ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, asset->mesh.ibo);
	gl.buffer_data(
			GL_ELEMENT_ARRAY_BUFFER,
			sizeof(u32) * asset->mesh.data.num_indices,
			asset->mesh.data.indices,
			GL_STATIC_DRAW
	);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void gen_render_plane(game_asset *asset)
{
	f32 render_plane_pos_data[] =
	{
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f
	};

	f32 render_plane_uv_data[] =
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};

	u32 render_plane_index_data[] =
	{
		// #note ccw
		0, 1, 2,
		0, 2, 3
		/*
		// #note cw
		0, 2, 1,
		0, 3, 2
		*/
	};

	asset->type = ASSETTYPE_MESH;
	asset->mesh.data = (mesh_data){ 0 };

	asset->mesh.data.num_vertices = ARRAY_COUNT(render_plane_pos_data);
	asset->mesh.data.num_indices = ARRAY_COUNT(render_plane_index_data);

	asset->mesh.data.positions = ast_malloc(sizeof(f32) * ARRAY_COUNT(render_plane_pos_data));
	asset->mesh.data.uvs = ast_malloc(sizeof(f32) * ARRAY_COUNT(render_plane_uv_data));
	asset->mesh.data.indices = ast_malloc(sizeof(u32) * ARRAY_COUNT(render_plane_index_data));

	memcpy(
			asset->mesh.data.positions,
			render_plane_pos_data,
			sizeof(f32) * ARRAY_COUNT(render_plane_pos_data)
	);
	memcpy(
			asset->mesh.data.uvs,
			render_plane_uv_data,
			sizeof(f32) * ARRAY_COUNT(render_plane_uv_data)
	);
	memcpy(
			asset->mesh.data.indices,
			render_plane_index_data,
			sizeof(f32) * ARRAY_COUNT(render_plane_index_data)
	);

	gl.gen_vertex_arrays(1, &asset->mesh.vao);
	gl.bind_vertex_array(asset->mesh.vao);

	gl.gen_buffers(1, &asset->mesh.vbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, asset->mesh.vbo);
	gl.buffer_data(
			GL_ARRAY_BUFFER,
			3 * sizeof(f32) * asset->mesh.data.num_vertices,
			asset->mesh.data.positions,
			GL_STATIC_DRAW
	);
	gl.enable_vertex_attrib_array(0);
	gl.vertex_attrib_pointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);

	gl.gen_buffers(1, &asset->mesh.uvbo);
	gl.bind_buffer(GL_ARRAY_BUFFER, asset->mesh.uvbo);
	gl.buffer_data(
			GL_ARRAY_BUFFER,
			2 * sizeof(f32) * asset->mesh.data.num_vertices,
			asset->mesh.data.uvs,
			GL_STATIC_DRAW
	);
	gl.vertex_attrib_pointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);
	gl.enable_vertex_attrib_array(1);

	gl.bind_vertex_array(0);
	gl.bind_buffer(GL_ARRAY_BUFFER, 0);

	gl.gen_buffers(1, &asset->mesh.ibo);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, asset->mesh.ibo);
	gl.buffer_data(
			GL_ELEMENT_ARRAY_BUFFER,
			sizeof(u32) * asset->mesh.data.num_indices,
			asset->mesh.data.indices,
			GL_STATIC_DRAW
	);
	gl.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	CHECK_GL_ERROR;
}

// #todo there should be a better way to control the light data, such as simply not binding
// anything and setting it later in the renderer init.
void load_assets(game_assets *assets, deferred_renderer *renderer)
{
	// #note these are built-in assets
	gen_render_plane(&assets->assets[ASSET_RENDER_PLANE_MESH]);
	gen_axes(assets);
	gen_lamps(assets);
	//gen_default_skybox(assets);

	// #note loaded assets
	file_contents assets_fc = ast_read_raw_file("asset_pak.blk");
	u8 *assets_ptr = (u8 *)assets_fc.data;
	// #todo consider using a struct or array for magic number
	if (!ast_strcmp("BLEAK_ASSET_PACK", (char *)assets_ptr))
	{
		printf("Incorrect or corrupted file\n");
		exit(1);
	}
	assets_ptr += 16;

	while (assets_ptr - (u8 *)assets_fc.data < (s64)assets_fc.size)
	{
		game_asset_type type = *CONSUME(assets_ptr, game_asset_type);
		u32 length = *CONSUME(assets_ptr, u32);
		switch (type)
		{
			case ASSETTYPE_TEXTURE:
			{
				gen_texture_asset(assets, assets_ptr);
			} break;
			case ASSETTYPE_MESH:
			{
				gen_mesh_asset(assets, assets_ptr);
			} break;
			case ASSETTYPE_SHADER:
			{
				gen_shader_asset(assets, assets_ptr, renderer);
			} break;
			case ASSETTYPE_SOUND:
			case ASSETTYPE_INVALID:
			default:
			{
				ASSERT(0);
			} break;
		}
		assets_ptr += length;
	}
	
	/*
	//raw_image diffuse = apl_parse_png("albedo_metallic_map.png");
	raw_image diffuse = apl_parse_png("albedo_metallic_map.png");
	raw_image normal = apl_parse_png("normal_roughness_map.png");
	mesh_data cube = read_dae("cube.dae");
	mesh_data plane = read_dae("plane.dae");
	mesh_data sphere = read_dae("sphere.dae");
	raw_image normal_plane = apl_parse_png("plane_normal_map.png");
	raw_image normal_sphere = apl_parse_png("sphere_normal_map.png");

	raw_image white = apl_parse_png("white.png");
	raw_image normal_plain = apl_parse_png("normal.png");

	gen_texture_asset(&assets->assets[ASSET_TEXTURE_NORMAL_PLAIN], normal_plain);
	gen_texture_asset(&assets->assets[ASSET_TEXTURE_WHITE], white);

	gen_texture_asset(&assets->assets[ASSET_TEXTURE_NORMAL_PLANE], normal_plane);
	gen_texture_asset(&assets->assets[ASSET_TEXTURE_NORMAL_SPHERE], normal_sphere);
	gen_mesh_asset(&assets->assets[ASSET_MESH_PLANE], plane);
	gen_mesh_asset(&assets->assets[ASSET_MESH_SPHERE], sphere);


	// #note #hack currently the indices are all predetermined values and will not work procedurally
	gen_texture_asset(&assets->assets[ASSET_TEXTURE_DIFFUSE_CUBE], diffuse);
	gen_texture_asset(&assets->assets[ASSET_TEXTURE_NORMAL_CUBE], normal);

	gen_mesh_asset(&assets->assets[ASSET_MESH_CUBE], cube);

	char *vertex_shader_source = ast_read_raw_file("shaders/gbuffer.vs").data;
	char *fragment_shader_source = ast_read_raw_file("shaders/gbuffer.fs").data;
	ASSERT(vertex_shader_source);
	ASSERT(fragment_shader_source);
	gen_shader_asset(
			ASSET_GBUFFER_SHADER,
			renderer,
			&assets->assets[ASSET_GBUFFER_SHADER],
			vertex_shader_source,
			fragment_shader_source
	);

	vertex_shader_source = ast_read_raw_file("shaders/deferred_shading.vs").data;
	fragment_shader_source = ast_read_raw_file("shaders/deferred_shading.fs").data;
	ASSERT(vertex_shader_source);
	ASSERT(fragment_shader_source);
	gen_shader_asset(
			ASSET_DEFERRED_SHADER,
			renderer,
			&assets->assets[ASSET_DEFERRED_SHADER],
			vertex_shader_source,
			fragment_shader_source
	);
	*/
}

#undef CONSUME

texture_id get_texture_id(game_assets *assets, asset_specifier id)
{
	texture_id result = { 0 };

	// #todo #hack this is only while a better system for specifying assets has not been implemented
	result.asset_index = id;
	game_asset asset = assets->assets[id];

	// #debug seems to erroneously trigger
	//ASSERT(asset.type == ASSETTYPE_TEXTURE);
	result.tex_id = asset.texture.texture_id;

	return result;
}

mesh_id get_mesh_id(game_assets *assets, asset_specifier id)
{
	mesh_id result = { 0 };

	// #todo #hack this is only while a better system for specifying assets has not been implemented
	result.asset_index = id;
	game_asset asset = assets->assets[id];

	// #debug seems to erroneously trigger
	//ASSERT(asset.type == ASSETTYPE_MESH);
	result.vao = asset.mesh.vao;
	result.ibo = asset.mesh.ibo;
	result.num_indices = asset.mesh.data.num_indices;

	return result;
}

glshader *get_glshader(game_assets *assets, asset_specifier id)
{
	game_asset *asset = assets->assets + id;
	ASSERT(asset->type == ASSETTYPE_SHADER);
	return &asset->shader;
}
