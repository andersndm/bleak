#!/bin/bash

ctags -e ./*.h ./*.c ../../ast/*.h ../../apl/*.h

# 0 for debug, 1 for release
config=0

exe_name=linux_bleak
so_name=bleak
pak_name=pak_bleak

exe_inc="-I../../ast/ -I../../apl"
so_inc="-I../../ast/ -I../../apl"
pak_inc="-I../../ast/ -I../../apl"

exe_libs="-L../../apl/lib/ -lapl -lm -lX11 -lGL -lGLU -lasound -ldl"
pak_libs="-lm -pthread"

wrng_ignore="-Wno-missing-braces -Wno-unused-parameter -Wno-unused-variable"
wrng="-Wall -Wextra $wrng_ignore"

if [ $config -eq 0 ]; then
	echo "debug build"
	debug_vars="-g -DDEBUG_PRINT=0"
	opt=""
else
	echo "release build"
	debug_vars="-g"
	opt="-O3"
fi

if [ ! -d ../data ] ; then
	mkdir ../data
fi

common_cflags="-std=gnu11 $debug_vars $wrng $opt"

clang $common_cflags $exe_inc engine_sys.c -o ../data/$exe_name $exe_libs

clang -shared -fPIC $common_cflags $so_inc host.c -o ../data/lib$so_name.so

clang $common_cflags $pak_inc asset_packer.c -o ../data/$pak_name $pak_libs

exe_comm="\"clang $common_cflags $exe_inc engine_sys.c -o ../data/$exe_name $exe_libs\""
so_comm="\"clang -shared -fPIC $common_cflags $so_inc host.c -o ../data/lib$so_name.so\""
pak_comm="\"clang $common_cflags $pak_inc asset_packer.c -o ../data/$pak_name $pak_libs\""

dir="    \"directory\": \"$PWD\",\n"
comm="    \"command\": "
file="    \"file\": "
comp_exe="{\n$dir$comm$exe_comm\n$file\"engine_sys.c\"\n  },\n"
comp_so="{\n$dir$comm$so_comm\n$file\"host.c\"\n  },\n"
comp_pak="{\n$dir$comm$pak_comm\n$file\"asset_packer.c\"\n  }\n"
comp_db="[\n  $comp_exe  $comp_so  $comp_pak]"
echo -e $comp_db > compile_commands.json

pushd ../data > /dev/null
echo -e "\nrunning pak_bleak..."
#./pak_bleak
popd > /dev/null
