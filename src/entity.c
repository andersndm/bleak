entity *add_camera(game_state *state, vec3 position, f32 fov)
{
	ASSERT(state->num_entities < MAX_ENTITIES);
	entity *result = state->entities + state->num_entities++;
	result->type = ENTITYTYPE_CAMERA3D;
	result->transform.position = position;
	result->transform.rotation = q4_identity();
	result->transform.scale = v3(1.0f, 1.0f, 1.0f);
	result->camera.fov = fov;
	result->camera.aspect_ratio = (f32)state->sys.client_width / (f32)state->sys.client_height;
	result->camera.view = m4_identity();
	result->camera.proj = m4_identity();
	return result;
}

// #todo this is not really an entity type but a draw type, once proper entities are added remove
entity *add_static_mesh(
		game_state *state,
		render_mesh_type type,
		u16 flags,
		vec3 position,
		vec3 scale,
		quaternion rotation,
		vec3 color,
		texture_id diffuse,
		texture_id normal,
		mesh_id mesh
	)
{
	ASSERT(state->num_entities < MAX_ENTITIES);
	entity *result = state->entities + state->num_entities++;
	result->type = ENTITYTYPE_STATIC_MESH;
	result->transform.position = position;
	result->transform.scale = scale;
	result->transform.rotation = rotation;
	result->static_mesh.type = type;
	result->static_mesh.flags = flags;
	result->static_mesh.color = color;
	result->static_mesh.diffuse_texture_id = diffuse;
	result->static_mesh.normal_texture_id = normal;
	result->static_mesh.mesh_id = mesh;
	return result;
}

entity *add_dir_lamp(game_state *state, quaternion direction, vec3 color, f32 intensity)
{
	ASSERT(state->num_entities < MAX_ENTITIES);
	entity *result = state->entities + state->num_entities++;
	result->type = ENTITYTYPE_DIR_LAMP;
	result->transform.position = v3_zero();
	result->transform.scale = v3(1.0f, 1.0f, 1.0f);
	result->transform.rotation = direction;
	result->dir_lamp.color = color;
	result->dir_lamp.intensity = intensity;
#if 1
	result->dir_lamp.lamp = add_static_mesh(
		state,
		RENDERMESH_LINES,
		RENDERFLAGS_SHADELESS,
		v3_zero(),
		v3(1.0f, 1.0f, 1.0f),
		direction,
		color,
		get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
		get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_PLANE),
		get_mesh_id(&state->assets, ASSET_MESH_DIR_LAMP)
		);
#else
	result->point_lamp.lamp = add_static_mesh(
			state,
			RENDERMESH_LINES,
			RENDERFLAGS_SHADELESS,
			v3_zero(),
			v3(10.0f, 10.0f, 10.0f),
			q4_identity(),
			v3(1.0f, 0.0f, 0.0f), //color,
			get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_SPHERE),
			get_mesh_id(&state->assets, ASSET_MESH_DIR_LAMP)
		);
#endif
	return result;
}

entity *add_point_lamp(game_state *state, vec3 position, vec3 color, f32 intensity)
{
	ASSERT(state->num_entities < MAX_ENTITIES);
	entity *result = state->entities + state->num_entities++;
	result->type = ENTITYTYPE_POINT_LAMP;
	result->transform.position = position;
	result->transform.scale = v3(1.0f, 1.0f, 1.0f);
	result->transform.rotation = q4_identity();
	result->point_lamp.color = color;
	result->point_lamp.intensity = intensity;
	result->point_lamp.lamp = add_static_mesh(
			state,
			RENDERMESH_TRIANGLES,
			RENDERFLAGS_SHADELESS,
			position,
			v3(0.1f, 0.1f, 0.1f),
			q4_identity(),
			color,
			get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
			get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_SPHERE),
			get_mesh_id(&state->assets, ASSET_MESH_SPHERE)
	);
	return result;
}

entity *add_spot_lamp(
	game_state *state,
	vec3 position,
	quaternion rotation,
	vec3 color,
	f32 inner_cutoff,
	f32 outer_cutoff,
	f32 intensity
	)
{
	ASSERT(state->num_entities < MAX_ENTITIES);
	entity *result = state->entities + state->num_entities++;
	result->type = ENTITYTYPE_SPOT_LAMP;
	result->transform.position = position;
	result->transform.scale = v3(1.0f, 1.0f, 1.0f);
	result->transform.rotation = rotation;
	result->spot_lamp.color = color;
	result->spot_lamp.intensity = intensity;
	result->spot_lamp.inner_cutoff = inner_cutoff;
	result->spot_lamp.outer_cutoff = outer_cutoff;
	result->spot_lamp.lamp = add_static_mesh(
		state,
		RENDERMESH_TRIANGLES,
		RENDERFLAGS_SHADELESS,
		position,
		v3(0.1f, 0.1f, 0.1f),
		rotation,
		color,
		get_texture_id(&state->assets, ASSET_TEXTURE_WHITE),
		get_texture_id(&state->assets, ASSET_TEXTURE_NORMAL_SPHERE),
		get_mesh_id(&state->assets, ASSET_MESH_SPHERE)
		);
	return result;
}

void update_camera_entity(entity* cam_entity)
{
	ASSERT(cam_entity->type == ENTITYTYPE_CAMERA3D);
	camera_3d *cam = &cam_entity->camera;
	cam->proj = m4_perspective(cam->fov, cam->aspect_ratio, 0.1f, 2000.0f);
	mat4 rot_mat = m4_rotation_quaternion(q4_inverse(cam_entity->transform.rotation));
	mat4 trans_mat = m4_translation(v3_mul(-1.0f, cam_entity->transform.position));
	cam->view = m4_mul_m4(rot_mat, trans_mat);
}
