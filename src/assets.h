#ifndef ASSETS_H
#define ASSETS_H

/*	#hack: once an actual asset pack is implemented a different system for requesting and loading
	assets will be implemented. Probably a way of specifying prefab objects is a good idea */

typedef enum asset_specifier
{
	// #todo the 0th asset should probably be a sentinel
	// #note plane to render gbuffer to
	ASSET_RENDER_PLANE_MESH,

	// #note shaders
	ASSET_DEPTH_SHADER,
	ASSET_SKYBOX_SHADER,
	ASSET_GBUFFER_SHADER,
	ASSET_SSAO_SHADER,
	ASSET_DEFERRED_SHADER,

	// #note textures
	ASSET_TEXTURE_WHITE,

	ASSET_TEXTURE_DIFFUSE_CUBE,

	ASSET_TEXTURE_NORMAL_PLAIN,
	ASSET_TEXTURE_NORMAL_PLANE,
	ASSET_TEXTURE_NORMAL_SPHERE,
	ASSET_TEXTURE_NORMAL_CUBE,

	ASSET_TEXTURE_DIFFUSE_CHARACTER,
	ASSET_TEXTURE_NORMAL_CHARACTER,

	ASSET_TEXTURE_SKYBOX,
	ASSET_CUBEMAP_DEFAULT_SKYBOX,
	ASSET_CUBEMAP_DAY_SKYBOX,
	ASSET_CUBEMAP_TWILIGHT_SKYBOX,
	ASSET_CUBEMAP_NIGHT_SKYBOX,

	// #note meshes
	// #todo use a single mesh and just transform them
	ASSET_MESH_POSXAXIS,
	ASSET_MESH_NEGXAXIS,
	ASSET_MESH_POSYAXIS,
	ASSET_MESH_NEGYAXIS,
	ASSET_MESH_POSZAXIS,
	ASSET_MESH_NEGZAXIS,

	ASSET_MESH_DIR_LAMP,

	ASSET_MESH_CUBE,
	ASSET_MESH_SPHERE,
	ASSET_MESH_SPHERE_HP,
	ASSET_MESH_PLANE,
	ASSET_MESH_CHARACTER,

	NUM_ASSETS
} asset_specifier;

typedef enum game_asset_type
{
	ASSETTYPE_INVALID,
	ASSETTYPE_TEXTURE,
	ASSETTYPE_CUBE_MAP,
	ASSETTYPE_MESH,
	ASSETTYPE_SOUND,
	ASSETTYPE_SHADER
} game_asset_type;

typedef struct asset_header
{
	game_asset_type type;
	u64 length;
} asset_header;

typedef struct texture_id
{
	u32 asset_index;
	u32 tex_id;
} texture_id;

typedef struct mesh_id
{
	u32 asset_index;
	u32 vao;
	u32 ibo;
	u32 num_indices;
} mesh_id;

typedef struct sound_id
{
	u32 asset_index;
} sound_id;

typedef struct texture_data
{
	u32 width;
	u32 height;
	u8 *pixels;
} texture_data;

typedef struct mesh_data
{
	u32 num_vertices;
	u32 num_indices;

	f32 *positions;
	f32 *normals;
	f32 *tangents;
	f32 *uvs;
	u32 *indices;
} mesh_data;

typedef struct glshader
{
	char *vertex_source;
	char *fragment_source;
	u32 program;
	union
	{
		struct
		{
			u32 projection_id;
			u32 view_id;

			u32 albedo_sampler_id;
			u32 color_id;
		} skybox;
		struct
		{
			u32 projection_id;
			u32 view_id;
			u32 model_id;

			u32 albedo_metallic_sampler_id;
			u32 normal_roughness_sampler_id;
			u32 object_color_id;
			u32 emissive_id;
		} gbuffer;
		struct
		{
			u32 albedo_metallic_sampler_id;
			u32 normal_roughness_sampler_id;
			u32 pos_sampler_id;

			u32 camera_pos_id;

			u32 ambient_light_id;

			u32 dir_lights_block_id;
			u32 num_dir_lights_id;
			u32 dir_lights_ssbo;
			u32 dir_lights_binding_id;

			u32 point_lights_block_id;
			u32 num_point_lights_id;
			u32 point_lights_ssbo;
			u32 point_lights_binding_id;

			u32 spot_lights_block_id;
			u32 num_spot_lights_id;
			u32 spot_lights_ssbo;
			u32 spot_lights_binding_id;

			u32 output_id;
		} deferred;
	};
} glshader;

typedef struct game_asset
{
	game_asset_type type;
	/*	#todo need an asset memory arena, the memory requirements for different types of assets are
		unbalanced and pointers should probably be used instead to minimize wasted space */
	union
	{
		struct
		{
			u32 texture_id;
			texture_data data;
		} texture;
		struct
		{
			u32 cubemap_id;
			// #note width and height is per texture, not entire cubemap
			u32 width;
			u32 height;
			u8 *side_pixels;
			u8 *up_pixels;
			u8 *down_pixels;
		} cubemap;
		struct
		{
			u32 vao;
			u32 vbo;
			u32 uvbo;
			u32 nbo;
			u32 tbo;
			u32 ibo;

			mesh_data data;
		} mesh;
		struct
		{
			/* #todo this struct is only filled to pass empty struct requirements, in no way is it
			   completed */
			s32 num_channels;
		} sound;
		glshader shader;
	};
} game_asset;

//#define MAX_GAME_ASSETS 32
typedef struct game_assets
{
	u32 num_assets;
	game_asset assets[NUM_ASSETS];
} game_assets;

#endif // !ASSETS_H
