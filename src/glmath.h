#ifndef BLEAK_GLMATH_H
#define BLEAK_GLMATH_H

#include <ast.h>

#ifdef _WIN32
//#include <cmath>
#include <math.h>
#include <float.h>
#else
#include <float.h>
#include <math.h>
#endif

#define PI 3.14159265359f
#define PI_05 1.570796326795f
#define PI_2 6.28318530718f

#define F32_TOLERANCE 0.00001f

/* }}}
--------------------------------------------------------------------------------
	SECTION: Type Declarations
--------------------------------------------------------------------------------
{{{ */

typedef union vector2D
{
	f32 e[2];
	struct
	{
		f32 x, y;
	};
} vec2;

typedef union vector3D
{
	f32 e[3];
	struct
	{
		f32 x, y, z;
	};
	struct
	{
		f32 r, g, b;
	};
} vec3;

typedef union vector4D
{
	f32 e[4];
	struct
	{
		f32 x, y, z, w;
	};
	struct
	{
		f32 r, g, b, a;
	};
	struct
	{
		vec2 xy;
		vec2 zw;
	};
	struct
	{
		vec3 rgb;
	};
} vec4, color, quaternion;

typedef union matrix4x4
{
	f32 e[16];
	struct
	{
		f32 a11, a21, a31, a41;
		f32 a12, a22, a32, a42;
		f32 a13, a23, a33, a43;
		f32 a14, a24, a34, a44;
	};
} mat4;

typedef struct rectangle
{
	vec2 pos;
	vec2 dim;
} rect;

/* }}}
--------------------------------------------------------------------------------
	SECTION: Scalar Operators
--------------------------------------------------------------------------------
{{{ */

static inline s32 abss32(s32 value)
{
	if(value < 0)
	{
		return -value;
	}
	return value;
}

static inline f32 absf32(f32 value)
{
	if(value < 0.0f)
	{
		return -value;
	}
	return value;
}

static inline f32 minf32(f32 a, f32 b)
{
	if (a > b)
	{
		return b;
	}
	return a;
}

static inline f32 maxf32(f32 a, f32 b)
{
	if (a < b)
	{
		return b;
	}
	return a;
}

static inline f32 sqrtf32(f32 value)
{
#ifdef _WIN32
	return sqrtf(value);
#else
	return sqrt(value);
#endif
}

f32 to_radians(f32 deg)
{
	return deg * PI / 180.0f;
}

static inline f32 sinf32(f32 angle)
{
#ifdef _WIN32
	return sinf(angle);
#else
	return sin(angle);
#endif
}

static inline f32 cosf32(f32 angle)
{
#ifdef _WIN32
	return cosf(angle);
#else
	return cos(angle);
#endif
}

static inline f32 tanf32(f32 angle)
{
#ifdef _WIN32
	return tanf(angle);
#else
	return tan(angle);
#endif
}

static inline f32 floorf32(f32 value)
{
	f32 result = 0.0f;
	if(value < 0.0f)
	{
		result = ceilf(value);
	}
	else
	{
		result = floorf(value);
	}
	return result;
}

static inline f32 clampf32(f32 val, f32 min, f32 max)
{
	if (val < min) { return min; }
	if (val > max) { return max; }
	return val;
}

static inline f32 lerp(f32 start, f32 end, f32 t)
{
	f32 result = start + t * (end - start);
	return result;
}

static inline f32 smooth_start2(f32 t)
{
	// NOTE: Ease in quadratic
	f32 result = t * t;
	return result;
}

static inline f32 smooth_start3(f32 t)
{
	// NOTE: Ease in cubic
	f32 result = t * t * t;
	return result;
}

static inline f32 smooth_start4(f32 t)
{
	// NOTE: Ease in quartic
	f32 result = t * t * t * t;
	return result;
}

static inline f32 smooth_stop2(f32 t)
{
	// NOTE: Ease out quadratic
	f32 result = 1.0f - t;
	result = result * result;
	result = 1.0f - result;
	return result;
}

static inline f32 smooth_stop3(f32 t)
{
	// NOTE: Ease out quadratic
	f32 result = 1.0f - t;
	result = result * result * result;
	result = 1.0f - result;
	return result;
}

static inline f32 smooth_stop4(f32 t)
{
	// NOTE: Ease out quadratic
	f32 result = 1.0f - t;
	result = result * result * result * result;
	result = 1.0f - result;
	return result;
}

static inline f32 smooth_step2(f32 t)
{
	//f32 result = lerp(smooth_start2(t), smooth_stop2(t), t);
	f32 result = 3 * t * t - 2 * t * t * t;
	return result;
#if 0
	if (t < 0.5f)
	{
		return smooth_start2(t);
	}
	else
	{
		return smooth_stop2(t);
	}
#endif
}

static inline f32 smooth_step3(f32 t)
{
	if (t < 0.5f)
	{
		return smooth_start3(t);
	}
	else
	{
		return smooth_stop3(t);
	}
}

static inline f32 smooth_step4(f32 t)
{
	if (t < 0.5f)
	{
		return smooth_start4(t);
	}
	else
	{
		return smooth_stop4(t);
	}
}

static inline f32 smooth_damp2(f32 current, f32 target, f32 *vel, f32 inc)
{
	*vel += inc;
	if (*vel > 1.0f)
	{
		*vel = 1.0f;
	}
	f32 t = smooth_step2(*vel);
	f32 result = lerp(current, target, t);
	result = t;
	return result;
}

static inline f32 smooth_damp3(f32 current, f32 target, f32 *vel, f32 inc)
{
	*vel += inc;
	f32 t = smooth_step3(*vel);
	f32 result = lerp(current, target, t);
	return result;
}

static inline f32 smooth_damp4(f32 current, f32 target, f32 *vel, f32 inc)
{
	*vel += inc;
	f32 t = smooth_step4(*vel);
	f32 result = lerp(current, target, t);
	return result;
}

static inline f32 smooth_damp(float current, float target, float *vel,
		float smooth_time, float max_speed, float dt)
{
	smooth_time = maxf32(0.0001f, smooth_time);
	f32 num = 2.0f / smooth_time;
	f32 num2 = num * dt;
	f32 num3 = 1.0f / (1.0f + num2 + 0.48f * num2 * num2 +
			0.235f * num2 * num2 * num2);
	f32 num4 = current - target;
	f32 num5 = target;
	f32 num6 = max_speed * smooth_time;
	num4 = clampf32(num4, -num6, num6);
	target = current - num4;
	f32 num7 = (*vel + num * num4) * dt;
	*vel = (*vel - num * num7) * num3;
	f32 num8 = target + (num4 + num7) * num3;
	if (num5 - current > 0.0f && num8 > num5)
	{
		num8 = num5;
		*vel = (num8 - num5) / dt;
	}
	return num8;
}

/* }}}
--------------------------------------------------------------------------------
	SECTION: 2D Vector Operations
--------------------------------------------------------------------------------
{{{ */

static inline vec2 v2(f32 x, f32 y)
{
	vec2 result;
	result.x = x; result.y = y;
	return result;
}

static inline vec2 v2_zero(void)
{
	return v2(0.0f, 0.0f);
}

static inline vec2 v2_scale(f32 s, vec2 u)
{
	return v2(s * u.x, s * u.y);
}

static inline vec2 v2_add(vec2 u, vec2 v)
{
	return v2(u.x + v.x, u.y + v.y);
}

static inline vec2 v2_sub(vec2 u, vec2 v)
{
	return v2(u.x - v.x, u.y - v.y);
}

static inline vec2 v2_neg(vec2 u)
{
	return v2(-u.x, -u.y);
}

static inline f32 v2_magnitude_sq(vec2 v)
{
	return v.x * v.x + v.y * v.y;
}

static inline f32 v2_magnitude(vec2 v)
{
	return sqrtf32(v2_magnitude_sq(v));
}

static inline vec2 v2_normal(vec2 v)
{
	f32 mag = v2_magnitude(v);
	vec2 result = v2_zero();
	if(mag == 0.0f)
	{
		return result;
	}

	result.x = v.x / mag;
	result.y = v.y / mag;

	return result;
}

static inline f32 v2_dot(vec2 u, vec2 v)
{
	return u.x * v.x + u.y * v.y;
}

static inline vec2 v2_rotate(vec2 u, f32 angle)
{
	return v2(
		u.x * cosf(angle) - u.y * sinf(angle),
		u.x * sinf(angle) + u.y * cosf(angle)
	);
}

static inline vec2 absv2(vec2 u)
{
	return v2(absf32(u.x), absf32(u.y));
}

/* }}}
--------------------------------------------------------------------------------
	SECTION: 3D Vector Operations
--------------------------------------------------------------------------------
{{{ */

static inline vec3 v3(f32 x, f32 y, f32 z)
{
	vec3 result;
	result.x = x; result.y = y; result.z = z;
	return result;
}

static inline vec3 v3_zero(void)
{
	return v3(0.0f, 0.0f, 0.0f);
}

static inline vec3 v3_scale(f32 s, vec3 u)
{
	return v3(s * u.x, s * u.y, s * u.z);
}

static inline vec3 v3_add(vec3 u, vec3 v)
{
	return v3(u.x + v.x, u.y + v.y, u.z + v.z);
}

static inline vec3 v3_sub(vec3 u, vec3 v)
{
	return v3(u.x - v.x, u.y - v.y, u.z - v.z);
}

static inline vec3 v3_neg(vec3 u)
{
	return v3(-u.x, -u.y, -u.z);
}

static inline f32 v3_magnitude_sq(vec3 v)
{
	return v.x * v.x + v.y * v.y + v.z * v.z;
}

static inline f32 v3_magnitude(vec3 v)
{
	return sqrtf32(v3_magnitude_sq(v));
}

static inline vec3 v3_normal(vec3 v)
{
	f32 mag = v3_magnitude(v);
	vec3 result = v3_zero();
	if(mag == 0.0f)
	{
		return result;
	}

	result.x = v.x / mag;
	result.y = v.y / mag;
	result.z = v.z / mag;

	return result;
}

static inline vec3 v3_cross(vec3 u, vec3 v)
{
	vec3 result = v3_zero();

	result.x = u.y * v.z - u.z * v.y;
	result.y = u.z * v.x - u.x * v.z;
	result.z = u.x * v.y - u.y * v.x;

	return result;
}

static inline f32 v3_dot(vec3 u, vec3 v)
{
	return u.x * v.x + u.y * v.y + u.z * v.z;
}

/* }}}
--------------------------------------------------------------------------------
	SECTION: 4D Vector Operations
--------------------------------------------------------------------------------
{{{ */

static inline vec4 v4(f32 x, f32 y, f32 z, f32 w)
{
	vec4 result;
	result.x = x; result.y = y; result.z = z; result.w = w;
	return result;
}

static inline vec4 v4_zero(void)
{
	return v4(0.0f, 0.0f, 0.0f, 0.0f);
}

/* }}}
--------------------------------------------------------------------------------
	SECTION: Matrix Operations
--------------------------------------------------------------------------------
{{{ */

static inline mat4 m4_zero(void)
{
	mat4 result =
	{
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f
	};
	return result;
}
static inline mat4 m4_identity_matrix(void)
{
	mat4 result = m4_zero();
	result.a11 = 1.0f;
	result.a22 = 1.0f;
	result.a33 = 1.0f;
	result.a44 = 1.0f;

	return result;
}

static inline mat4 m4_perspective(f32 fov, f32 aspect_ratio, f32 znear, f32 zfar)
{
	mat4 result = m4_zero();
	f32 tfov = tanf32(fov * 0.5f);

	result.a11 = 1.0f / (tfov * aspect_ratio);
	result.a22 = 1.0f / tfov;
	result.a33 = (zfar / (znear - zfar));
	result.a34 = -1.0f;
	result.a43 = -((2.0f * zfar * znear) / (zfar - znear));

	return result;
}

static inline mat4 m4_ortho(f32 left, f32 right, f32 bottom, f32 top, f32 znear, f32 zfar)
{
	mat4 result = m4_identity_matrix();

	result.a11 = 2.0f / (right - left);
	result.a22 = 2.0f / (top - bottom);
	result.a33 = -2.0f / (zfar - znear);
	result.a41 = -(right + left) / (right - left);
	result.a42 = -(top + bottom) / (top - bottom);
	result.a43 = -(zfar + znear) / (zfar - znear);

	return result;
}

static inline vec4 m4_mul_v4(mat4 m, vec4 v)
{
	vec4 result = v4_zero();

	result.x = m.a11 * v.x + m.a12 * v.y + m.a13 * v.z + m.a14 * v.w;
	result.y = m.a21 * v.x + m.a22 * v.y + m.a23 * v.z + m.a24 * v.w;
	result.z = m.a31 * v.x + m.a32 * v.y + m.a33 * v.z + m.a34 * v.w;
	result.w = m.a41 * v.x + m.a42 * v.y + m.a43 * v.z + m.a44 * v.w;

	return result;
}

static inline mat4 m4_scale_matrix(f32 x_scale, f32 y_scale, f32 z_scale)
{
	mat4 result = m4_zero();

	result.a11 = x_scale;
	result.a22 = y_scale;
	result.a33 = z_scale;
	result.a44 = 1.0f;

	return result;
}

static inline mat4 m4_translation_matrix(f32 x_pos, f32 y_pos, f32 z_pos)
{
	mat4 result = m4_identity_matrix();

	result.a41 = x_pos;
	result.a42 = y_pos;
	result.a43 = z_pos;

	return result;
}

static inline mat4 m4_x_rotation_matrix(f32 angle)
{
	mat4 result = m4_identity_matrix();
	return result;
}

static inline mat4 m4_y_rotation_matrix(f32 angle)
{
	mat4 result = m4_identity_matrix();
	return result;
}

static inline mat4 m4_z_rotation_matrix(f32 angle)
{
	mat4 result = m4_identity_matrix();
	return result;
}

static inline mat4 m4_rotation_matrix(f32 angle, vec3 v)
{
	mat4 result = m4_identity_matrix();

	f32 cosa = cosf32(angle);
	f32 sina = sinf32(angle);
	f32 icos = 1 - cosa;

	result.a11 =        cosa + v.x * v.x * icos;
	result.a12 = -v.z * sina + v.x * v.y * icos;
	result.a13 =  v.y * sina + v.x * v.z * icos;

	result.a21 =  v.z * sina + v.y * v.x * icos;
	result.a22 =        cosa + v.y * v.y * icos;
	result.a23 = -v.x * sina + v.y * v.z * icos;

	result.a31 = -v.y * sina + v.z * v.x * icos;
	result.a32 =  v.x * sina + v.z * v.y * icos;
	result.a33 =        cosa + v.z * v.z * icos;

	return result;
}

static inline mat4 m4_mul_m4(mat4 a, mat4 b)
{
	mat4 result = m4_zero();

	result.a11 = b.a11*a.a11 + b.a12*a.a21 + b.a13*a.a31 + b.a14*a.a41;
	result.a12 = b.a11*a.a12 + b.a12*a.a22 + b.a13*a.a32 + b.a14*a.a42;
	result.a13 = b.a11*a.a13 + b.a12*a.a23 + b.a13*a.a33 + b.a14*a.a43;
	result.a14 = b.a11*a.a14 + b.a12*a.a24 + b.a13*a.a34 + b.a14*a.a44;

	result.a21 = b.a21*a.a11 + b.a22*a.a21 + b.a23*a.a31 + b.a24*a.a41;
	result.a22 = b.a21*a.a12 + b.a22*a.a22 + b.a23*a.a32 + b.a24*a.a42;
	result.a23 = b.a21*a.a13 + b.a22*a.a23 + b.a23*a.a33 + b.a24*a.a43;
	result.a24 = b.a21*a.a14 + b.a22*a.a24 + b.a23*a.a34 + b.a24*a.a44;

	result.a31 = b.a31*a.a11 + b.a32*a.a21 + b.a33*a.a31 + b.a34*a.a41;
	result.a32 = b.a31*a.a12 + b.a32*a.a22 + b.a33*a.a32 + b.a34*a.a42;
	result.a33 = b.a31*a.a13 + b.a32*a.a23 + b.a33*a.a33 + b.a34*a.a43;
	result.a34 = b.a31*a.a14 + b.a32*a.a24 + b.a33*a.a34 + b.a34*a.a44;

	result.a41 = b.a41*a.a11 + b.a42*a.a21 + b.a43*a.a31 + b.a44*a.a41;
	result.a42 = b.a41*a.a12 + b.a42*a.a22 + b.a43*a.a32 + b.a44*a.a42;
	result.a43 = b.a41*a.a13 + b.a42*a.a23 + b.a43*a.a33 + b.a44*a.a43;
	result.a44 = b.a41*a.a14 + b.a42*a.a24 + b.a43*a.a34 + b.a44*a.a44;

	return result;
}

static inline mat4 m4_look_at(vec3 eye, vec3 tgt, vec3 up)
{
	vec3 f = v3_normal(v3_sub(tgt, eye));
	vec3 s = v3_normal(v3_cross(f, up));
	vec3 u = v3_cross(s, f);

	mat4 result = m4_identity_matrix();
	result.a11 = s.x;
	result.a21 = s.y;
	result.a31 = s.z;
	result.a12 = u.x;
	result.a22 = u.y;
	result.a32 = u.z;
	result.a13 = -f.x;
	result.a23 = -f.y;
	result.a33 = -f.z;
	result.a41 = -v3_dot(s, eye);
	result.a42 = -v3_dot(u, eye);
	result.a43 = v3_dot(f, eye);

	return result;
}

/* }}}
--------------------------------------------------------------------------------
	SECTION: Type Printers
--------------------------------------------------------------------------------
{{{ */

static inline void print_v2(vec2* v, const char *name)
{
	printf("| %.2f %.2f | %s\n",
			v->x, v->y, name == 0 ? "" : name);
}

static inline void print_v3(vec3* v, const char *name)
{
	printf("| %.2f %.2f %.2f | %s\n",
			v->x, v->y, v->z, name == 0 ? "" : name);
}

static inline void print_v4(vec4* v, const char *name)
{
	printf("| %.2f %.2f %.2f %.2f | %s\n",
			v->x, v->y, v->z, v->w, name == 0 ? "" : name);
}

static inline void print_m4(mat4* m, const char *name)
{
	printf("┏                         ┓\n");
	for(u32 i = 0; i < 4; ++i)
	{
		printf("┃ ");
		for(u32 j = 0; j < 4; ++j)
		{
			f32 value = *(m->e + i * 4 + j);
			if(value == 0.0f) { value = 0.0f; } // NOTE: remove -0.0 in printf
			if(value < 0.0f)
			{
				printf("%.2f ", value);
			}
			else
			{
				printf(" %.2f ", value);
			}
		}
		printf("┃");
		if(i == 0)
		{
			printf(" %s\n", name == 0 ? "" : name);
		}
		else
		{
			printf("\n");
		}
	}
	printf("┗                         ┛\n");
}

#endif// !BLEAK_GLMATH_H
