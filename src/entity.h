#ifndef ENTITY_H
#define ENTITY_H

typedef enum entity_type
{
	ENTITYTYPE_EMPTY,
	ENTITYTYPE_DIR_LAMP,
	ENTITYTYPE_POINT_LAMP,
	ENTITYTYPE_SPOT_LAMP,
	ENTITYTYPE_CAMERA3D,
	ENTITYTYPE_STATIC_MESH
} entity_type;

typedef struct camera_3d
{
	f32 fov;
	f32 aspect_ratio;

	mat4 view;
	mat4 proj;

	f32 yaw_angle;
	f32 pitch_angle;
} camera_3d;

typedef struct transform3d
{
	vec3 position;
	vec3 scale;
	quaternion rotation;
} transform3d;

typedef struct entity
{
	entity_type type;
	transform3d transform;
	// #todo do enough entity types have a color that it may be worth extracting it from the union?
	union
	{
		// #note lamps use the transform for position and rotation
		struct
		{
			vec3 color;
			f32 intensity;
			struct entity *lamp;
		} point_lamp;
		struct
		{
			vec3 color;
			f32 intensity;
			struct entity *lamp;
		} dir_lamp;
		struct
		{
			vec3 color;
			f32 intensity;
			f32 inner_cutoff;
			f32 outer_cutoff;
			struct entity *lamp;
		} spot_lamp;
		camera_3d camera;
		struct
		{
			render_mesh_type type;
			u16 flags;
			vec3 color;
			texture_id diffuse_texture_id;
			texture_id normal_texture_id;
			mesh_id mesh_id;
		} static_mesh;
	};
} entity;

#endif // !ENTITY_H
