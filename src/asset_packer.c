/* #todo it would be nice to multithread the asset pak generation
   - implement the threads

   - currently it seems like using indexed meshes is pointless,
   because there are no overlapping vertices. either there are some,
   and they aren't being caught, or smooth meshes don't allow for for
   much improvement.
*/


#define AST_IGNORE_SHARED_LIBRARY
#define AST_IMPLEMENTATION
#include <ast.h>
#include <ast_math.h>

#include <apl_png.h>

#ifndef PL_LINUX
#error "asset pak file generation is only supported on linux"
#endif

#include <fcntl.h>
#include <pthread.h>
#include <sys/stat.h>

#include "assets.h"

// -------------------------------------------------------------------------------------------------
// #section collada parsing
// -------------------------------------------------------------------------------------------------
// {{{

typedef enum dae_tag_type
{
	DAETAG_IGNORE,
	DAETAG_FLOAT_ARRAY,
	DAETAG_TRIANGLES
} dae_tag_type;

typedef struct dae_tag
{
	dae_tag_type type;
	char *start;
	char *end;
	char *identifier;
	u32 identifier_length;

	union
	{
		struct
		{
			char *id;
			u32 id_length;
			u32 count;
			f32 *array;
		} float_array;
		struct
		{
			u32 count;
			u32 *array;
		} triangles;
	};

} dae_tag;

char *skip_to_tag(char *data)
{
	while (*data && *data != '<')
	{
		++data;
	}
	return data;
}

char *skip_to_next_tag(char *data)
{
	return skip_to_tag(++data);
}

char *skip_to_char(char *data, char c)
{
	while(*data && *data != c)
	{
		++data;
	}
	return data;
}

dae_tag parse_tag(char *data)
{
	dae_tag result = { 0 };

	ASSERT(*data == '<');
	result.start = data;
	++data;
	// #note ignore xml version tag and end tags
	if (*data == '/' || *data == '?')
	{
		return result;
	}
	result.identifier = data;
	while (*data != ' ' && *data != '>')
	{
		++result.identifier_length;
		++data;
	}

	if (ast_strcmp("float_array", result.identifier))
	{
		result.type = DAETAG_FLOAT_ARRAY;
		data = skip_to_char(data, '"');
		data++;
		result.float_array.id = data;
		while (*data != '"')
		{
			++data;
			++result.float_array.id_length;
		}
		++data;
		data = skip_to_char(data, '"');
		++data;

		result.float_array.count = (u32)strtol(data, &data, 10);
		data += 2;

		result.float_array.array = ast_malloc(sizeof(f32) * result.float_array.count);
		for (u32 i = 0; i < result.float_array.count; ++i)
		{
			*(result.float_array.array + i) = strtof(data, &data);
		}
	}
	else if (ast_strcmp("triangles", result.identifier))
	{
		result.type = DAETAG_TRIANGLES;
		data = skip_to_char(data, '"');
		++data;
		data = skip_to_char(data, '"');
		++data;
		data = skip_to_char(data, '"');
		++data;
		result.triangles.count = (u32)strtol(data, &data, 10);

		while (!ast_strcmp("<p>", data))
		{
			++data;
			data = skip_to_char(data, '<');
		}

		data += 3;

		result.triangles.array = ast_malloc(sizeof(u32) * 3 * 3 * result.triangles.count);
		for (u32 i = 0, n = result.triangles.count * 3 * 3; i < n; ++i)
		{
			*(result.triangles.array + i) = strtoul(data, &data, 10);
		}
	}

	while (*data)
	{
		if (*data == '<' && *(data + 1) == '/')
		{
			data += 2;
			if (ast_strncmp(result.identifier, data, result.identifier_length))
			{
				result.end = data + result.identifier_length + 1;
				break;
			}
		}
		data = skip_to_next_tag(data);
	}

	return result;
}

mesh_data translate_to_mesh(
		u32 num_pos,
		f32 *pos,
		u32 num_normal,
		f32 *normal,
		u32 num_uv,
		f32 *uv,
		u32 num_idx,
		u32 *idx
	)
{
	/*	#note
		- read the three index values
		- compare the values with the others in the check array
		- if it's a match the index is the index in the check array
		- if it's not a match add it to the check array, the index is the index, and add the
			corresponding position, normal, and uvs to their arrays
	*/

	mesh_data result = { 0 };

	result.num_indices = num_idx * 3;
	result.indices = ast_malloc(sizeof(u32) * result.num_indices);

	f32 *positions = ast_malloc(sizeof(f32) * 3 * result.num_indices);
	f32 *normals = ast_malloc(sizeof(f32) * 3 * result.num_indices);
	f32 *uvs = ast_malloc(sizeof(f32) * 2 * result.num_indices);

	tuple3_u32 *check = ast_malloc(sizeof(tuple3_u32) * result.num_indices);
	u32 current_max_index = 0;

	for (u32 i = 0; i < result.num_indices; ++i)
	{
		tuple3_u32 current_idx = { idx[3 * i + 0], idx[3 * i + 1], idx[3 * i + 2] };
		b32 match_found = false;
		for (u32 j = 0; j < current_max_index; ++j)
		{
			if (
					current_idx.x == check[j].x &&
					current_idx.y == check[j].y &&
					current_idx.z == check[j].z
				)
			{
				match_found = true;
				result.indices[i] = j;
				break;
			}
		}

		if (!match_found)
		{
			check[current_max_index] = current_idx;
			result.indices[i] = current_max_index++;
			positions[3 * i + 0] = pos[3 * current_idx.x + 0];
			positions[3 * i + 1] = pos[3 * current_idx.x + 1];
			positions[3 * i + 2] = pos[3 * current_idx.x + 2];
			normals[3 * i + 0] = normal[3 * current_idx.y + 0];
			normals[3 * i + 1] = normal[3 * current_idx.y + 1];
			normals[3 * i + 2] = normal[3 * current_idx.y + 2];
			uvs[2 * i + 0] = uv[2 * current_idx.z + 0];
			uvs[2 * i + 1] = uv[2 * current_idx.z + 1];
		}
	}

	result.num_vertices = current_max_index;
	result.positions = ast_malloc(result.num_vertices * 3 * sizeof(f32));
	result.normals = ast_malloc(result.num_vertices * 3 * sizeof(f32));
	result.uvs = ast_malloc(result.num_vertices * 2 * sizeof(f32));

	for (u32 i = 0; i < result.num_vertices; ++i)
	{
		result.positions[3 * i + 0] = positions[3 * i + 0];
		result.positions[3 * i + 1] = positions[3 * i + 1];
		result.positions[3 * i + 2] = positions[3 * i + 2];
		result.normals[3 * i + 0] = normals[3 * i + 0];
		result.normals[3 * i + 1] = normals[3 * i + 1];
		result.normals[3 * i + 2] = normals[3 * i + 2];
		// #todo for some reason these are coming in a bit weird...
		result.uvs[2 * i + 0] = uvs[2 * i + 0];
		result.uvs[2 * i + 1] = 1.0f - uvs[2 * i + 1];
	}

#if 1 // #todo implement tangent calculation
	size_t tan_alloc_size = 3 * result.num_vertices * sizeof(f32);
	f32 *tan1 = ast_malloc(tan_alloc_size);
	//f32 *tan2 = ast_malloc(tan_alloc_size);
	for (u32 i = 0; i < result.num_indices; i += 3)
	{
		u32 i1 = result.indices[i + 0];
		u32 i2 = result.indices[i + 1];
		u32 i3 = result.indices[i + 2];

		f32 v1x = result.positions[3 * i1 + 0];
		f32 v1y = result.positions[3 * i1 + 1];
		f32 v1z = result.positions[3 * i1 + 2];
		f32 v2x = result.positions[3 * i2 + 0];
		f32 v2y = result.positions[3 * i2 + 1];
		f32 v2z = result.positions[3 * i2 + 2];
		f32 v3x = result.positions[3 * i3 + 0];
		f32 v3y = result.positions[3 * i3 + 1];
		f32 v3z = result.positions[3 * i3 + 2];

		f32 w1u = result.uvs[2 * i1 + 0];
		f32 w1v = result.uvs[2 * i1 + 1];
		f32 w2u = result.uvs[2 * i2 + 0];
		f32 w2v = result.uvs[2 * i2 + 1];
		f32 w3u = result.uvs[2 * i3 + 0];
		f32 w3v = result.uvs[2 * i3 + 1];

		f32 x1 = v2x - v1x;
		f32 x2 = v3x - v1x;
		f32 y1 = v2y - v1y;
		f32 y2 = v3y - v1y;
		f32 z1 = v2z - v1z;
		f32 z2 = v3z - v1z;

		f32 u1 = w2u - w1u;
		f32 u2 = w3u - w1u;
		f32 v1 = w2v - w1v;
		f32 v2 = w3v - w1v;

		f32 r = 1.0f / (u1 * v2 - u2 * v1);
		f32 udirx = r * (v2 * x1 - v1 * x2);
		f32 udiry = r * (v2 * y1 - v1 * y2);
		f32 udirz = r * (v2 * z1 - v1 * z2);
		#if 0
		f32 vdirx = r * (u1 * x2 - u2 * x1);
		f32 vdiry = r * (u1 * y2 - u2 * y1);
		f32 vdirz = r * (u1 * z2 - u2 * z1);
		#endif

#if 0
		ASSERT(udirx != 0.0f && udiry != 0.0f && udirz != 0.0f);
		ASSERT(vdirx != 0.0f && vdiry != 0.0f && vdirz != 0.0f);
#endif

		*(tan1 + 3 * i1 + 0) = udirx;
		*(tan1 + 3 * i1 + 1) = udiry;
		*(tan1 + 3 * i1 + 2) = udirz;
		*(tan1 + 3 * i2 + 0) = udirx;
		*(tan1 + 3 * i2 + 1) = udiry;
		*(tan1 + 3 * i2 + 2) = udirz;
		*(tan1 + 3 * i3 + 0) = udirx;
		*(tan1 + 3 * i3 + 1) = udiry;
		*(tan1 + 3 * i3 + 2) = udirz;

		/* #todo #debug getting a segmentation fault on writing to tan2. Pointers have been checked
		   and the access should be within the allocation for tan1. It only causes an issue on
		   loading the female character model. It causes the segfault on the first pass through the loop.

		   Should attempt with a smaller one, like the mannequin model.
		*/

		#if 0
		*(tan2 + 3 * i1 + 0) = vdirx;
		*(tan2 + 3 * i1 + 1) = vdiry;
		*(tan2 + 3 * i1 + 2) = vdirz;
		*(tan2 + 3 * i2 + 0) = vdirx;
		*(tan2 + 3 * i2 + 1) = vdiry;
		*(tan2 + 3 * i2 + 2) = vdirz;
		*(tan2 + 3 * i3 + 0) = vdirx;
		*(tan2 + 3 * i3 + 1) = vdiry;
		*(tan2 + 3 * i3 + 2) = vdirz;
		#endif
	}

	result.tangents = ast_malloc(3 * result.num_vertices * sizeof(f32));
	for (u32 i = 0; i < result.num_vertices; ++i)
	{
		f32 nx = result.normals[3 * i + 0];
		f32 ny = result.normals[3 * i + 1];
		f32 nz = result.normals[3 * i + 2];
		f32 tx = tan1[3 * i + 0];
		f32 ty = tan1[3 * i + 1];
		f32 tz = tan1[3 * i + 2];

		// #note gram-schmidt orthogonalize
		f32 ndott = nx * tx + ny * ty + nz * tz;
		f32 fx = tx - ndott * nx;
		f32 fy = ty - ndott * ny;
		f32 fz = tz - ndott * nz;

		f32 fmag = sqrtf32(fx * fx + fy * fy + fz * fz);
		result.tangents[3 * i + 0] = fx / fmag;
		result.tangents[3 * i + 1] = fy / fmag;
		result.tangents[3 * i + 2] = fz / fmag;
	}
#endif

	ast_free(tan1);
	ast_free(positions);
	ast_free(normals);
	ast_free(uvs);

	ast_free(check);
	return result;
}

mesh_data read_dae(const char *filename)
{
	file_contents fc = ast_read_raw_file(filename);
	char *data = fc.data;
	char *end = data + fc.size;

	u32 positions_count = 0;
	f32 *positions_array = 0;
	u32 normals_count = 0;
	f32 *normals_array = 0;
	u32 uvs_count = 0;
	f32 *uvs_array = 0;
	u32 index_count = 0;
	u32 *index_array = 0;

	while (*data)
	{
		if (*data == '<')
		{
			dae_tag tag = parse_tag(data);
			if (tag.identifier)
			{
				switch (tag.type)
				{
					case DAETAG_FLOAT_ARRAY:
					{
#if DEBUG_PRINT
						printf(
								"[FLOAT_ARRAY] %.*s [%u]\n",
								tag.float_array.id_length,
								tag.float_array.id,
								tag.float_array.count
						);
						printf("\t");
						for (u32 i = 0; i < tag.float_array.count; ++i)
						{
							f32 value = *(tag.float_array.array + i);
							if (value < 0.0f)
							{
								printf("%f ", value);
							}
							else
							{
								printf(" %f ", value);
							}
							if ((i + 1) % 8 == 0)
							{
								printf("\n\t");
							}
						}
						printf("\n");
#endif

						if (
								ast_nsubstring(
									"positions-array",
									tag.float_array.id,
									tag.float_array.id_length
								)
							)
						{
							ASSERT(positions_count == 0);
							ASSERT(positions_array == 0);
							positions_count = tag.float_array.count;
							positions_array = tag.float_array.array;
						}
						else if (
								ast_nsubstring(
									"normals-array",
									tag.float_array.id,
									tag.float_array.id_length
								)
							)
						{
							ASSERT(normals_count == 0);
							ASSERT(normals_array == 0);
							normals_count = tag.float_array.count;
							normals_array = tag.float_array.array;
						}
						else if (
								ast_nsubstring(
									"map-0-array",
									tag.float_array.id,
									tag.float_array.id_length
								)
							)
						{
							ASSERT(uvs_count == 0);
							ASSERT(uvs_array == 0);
							uvs_count = tag.float_array.count;
							uvs_array = tag.float_array.array;
						}
						else
						{
							ASSERT(0);
						}
					} break;
					case DAETAG_TRIANGLES:
					{
#if DEBUG_PRINT
						printf("[TRIANGLES] %u faces:\n", tag.triangles.count);
						for (u32 i = 0; i < tag.triangles.count; ++i)
						{
							u32 offset = 9 * i;
							u32 vertex = 3 * i;
							printf(
									"\t[%2u] "
									"v%2u: %2u/%2u/%2u "
									"v%2u: %2u/%2u/%2u "
									"v%2u: %2u/%2u/%2u\n",
									i,
									vertex,
									tag.triangles.array[offset + 0],
									tag.triangles.array[offset + 1],
									tag.triangles.array[offset + 2],
									vertex + 1,
									tag.triangles.array[offset + 3],
									tag.triangles.array[offset + 4],
									tag.triangles.array[offset + 5],
									vertex + 2,
									tag.triangles.array[offset + 6],
									tag.triangles.array[offset + 7],
									tag.triangles.array[offset + 8]
							);
						}
#endif

						index_count = tag.triangles.count;
						index_array = tag.triangles.array;
					} break;
					default:
					{
#if DEBUG_PRINT
						printf(
								"[DAETAG_IGNORE] %.*s - start: %lx end: %lx\n",
								tag.identifier_length,
								tag.identifier,
								(u64)tag.start,
								(u64)tag.end
						);
#endif
					} break;
				}
			}
		}
		data = skip_to_next_tag(data);
	}

	ASSERT(positions_count);
	ASSERT(positions_array);
	ASSERT(normals_count);
	ASSERT(normals_array);
	ASSERT(uvs_count);
	ASSERT(uvs_array);
	ASSERT(index_count);
	ASSERT(index_array);

	mesh_data result = translate_to_mesh(
			positions_count,
			positions_array,
			normals_count,
			normals_array,
			uvs_count,
			uvs_array,
			index_count,
			index_array
	);

	ast_free(positions_array);
	ast_free(normals_array);
	ast_free(uvs_array);
	ast_free(index_array);

	ast_free(fc.data);

	return result;
}

// }}}

typedef struct pak_data
{
	u8 *data;
	u64 write_offset;
	u64 capacity;
} pak_data;

typedef struct data_write
{
	u8 *ptr;
	u32 length;
} data_write;

typedef enum asset_work_kind
{
	ASSET_WORK_TEXTURE,
	ASSET_WORK_MESH,
	ASSET_WORK_SHADER
} asset_work_kind;

typedef struct asset_work
{
	asset_work_kind kind;
	asset_specifier spec;
	union
	{
		char *asset_filename;
		struct
		{
			char *vs_filename;
			char *fs_filename;
		} shader;
	};
} asset_work;

// #todo this will start as a very naive work queue, can be improved
typedef struct work_queue
{
	asset_work tasks[128];
	u32 num_tasks;
	u32 current_free_task;
	pak_data *data;
} work_queue;


void add_asset_to_queue(
	work_queue *queue,
	char *filename,
	game_asset_type type,
	asset_specifier spec
	)
{
	ASSERT(queue->num_tasks < ARRAY_COUNT(queue->tasks));
	asset_work *work = queue->tasks + queue->num_tasks;
	switch (type)
	{
	case ASSETTYPE_INVALID:
	{
		printf("error: adding invalid asset type to queue, ignoring\n");
		return;
	} break;
	case ASSETTYPE_TEXTURE:
	{
		work->kind = ASSET_WORK_TEXTURE;
	} break;
	case ASSETTYPE_CUBE_MAP:
	{
		printf("error: cubemaps are not currently supported, ignoring\n");
	} break;
	case ASSETTYPE_MESH:
	{
		work->kind = ASSET_WORK_MESH;
	} break;
	case ASSETTYPE_SOUND:
	{
		printf("error: sounds are not currently supported, ignoring\n");
		return;
	} break;
	case ASSETTYPE_SHADER:
	{
		printf("error: shaders have a dedicated queue adder, ignoring\n");
		return;
	} break;
	}
	++queue->num_tasks;

	work->asset_filename = filename;
	work->spec = spec;
}

void add_shader_to_queue(
	work_queue *queue,
	char *vs_filename,
	char *fs_filename,
	asset_specifier spec
	)
{
	ASSERT(queue->num_tasks < ARRAY_COUNT(queue->tasks));
	asset_work *work = queue->tasks + queue->num_tasks++;
	work->kind = ASSET_WORK_SHADER;
	work->spec = spec;
	work->shader.vs_filename = vs_filename;
	work->shader.fs_filename = fs_filename;
}

pthread_mutex_t write_data_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t queue_task_mutex = PTHREAD_MUTEX_INITIALIZER;

void write_data(pak_data *data, data_write dw)
{
	pthread_mutex_lock(&write_data_mutex);
	if (data->capacity - data->write_offset < dw.length)
	{
		u32 new_cap = (u32)((f32)(data->capacity + dw.length) * 1.5f);
		data->data = ast_realloc(data->data, new_cap);
		data->capacity = new_cap;
	}

	for (u32 i = 0; i < dw.length; ++i)
	{
		*(data->data + data->write_offset++) = *dw.ptr++;
	}
	pthread_mutex_unlock(&write_data_mutex);
}

data_write add_texture_asset(const char *tex_filename, asset_specifier spec)
{
	raw_image raw_texture = apl_parse_png(tex_filename);
	game_asset_type type = ASSETTYPE_TEXTURE;
	u32 length = sizeof(asset_specifier) + 2 * sizeof(u32) + 4 *
		raw_texture.width * raw_texture.height;

	data_write result;
	result.length = length + sizeof(game_asset_type) + sizeof(u32);
	result.ptr = ast_malloc(result.length);
	
	u8 *writer = result.ptr;
	*(game_asset_type *)writer = type;
	writer += sizeof(game_asset_type);
	*(u32 *)writer = length;
	writer += sizeof(u32);
	*(asset_specifier *)writer = spec;
	writer += sizeof(asset_specifier);
	*(u32 *)writer = raw_texture.width;
	writer += sizeof(u32);
	*(u32 *)writer = raw_texture.height;
	writer += sizeof(u32);
	u8 *tex_ptr = raw_texture.pixels;
	/* for (u32 i = 0; i < raw_texture.width * raw_texture.height; ++i) */
	/* { */
	/* 	*(u32 *)writer++ = *(u32 *)tex_ptr++; */
	/* } */
	for (u32 i = 0; i < raw_texture.width * raw_texture.height * 4; ++i)
	{
		*writer++ = *tex_ptr++;
	}
	ast_free(raw_texture.pixels);
	return result;

#if 0
	write(pak_fd, &type, sizeof(game_asset_type));
	// #todo does this need to be u64?
	// #todo apl_png needs to free memory
	
	write(pak_fd, &length, sizeof(u32));
	write(pak_fd, &spec, sizeof(asset_specifier));
	write(pak_fd, &raw_texture.width, sizeof(u32));
	write(pak_fd, &raw_texture.height, sizeof(u32));
	write(pak_fd, raw_texture.pixels, 4 * raw_texture.width * raw_texture.height);
	ast_free(raw_texture.pixels);
#endif
}

data_write add_static_mesh_asset(const char *mesh_filename, asset_specifier spec)
{
	mesh_data mesh = read_dae(mesh_filename);
	game_asset_type type = ASSETTYPE_MESH;
	u32 length =
		mesh.num_vertices * 11 * sizeof(f32) +
		mesh.num_indices * sizeof(u32) +
		2 * sizeof(u32) +
		sizeof(asset_specifier);

	data_write result;
	result.length = length + sizeof(game_asset_type) + sizeof(u32);
	result.ptr = ast_malloc(result.length);

	u8 *writer = result.ptr;
	*(game_asset_type *)writer = type;
	writer += sizeof(game_asset_type);
	*(u32 *)writer = length;
	writer += sizeof(u32);
	*(asset_specifier *)writer = spec;
	writer += sizeof(asset_specifier);
	*(u32 *)writer = mesh.num_vertices;
	writer += sizeof(u32);
	*(u32 *)writer = mesh.num_indices;
	writer += sizeof(u32);
	f32 *ptr = mesh.positions;
	for (u32 i = 0; i < mesh.num_vertices * 3; ++i)
	{
		*(f32 *)writer = *ptr++;
		writer += sizeof(f32);
	}
	ptr = mesh.normals;
	for (u32 i = 0; i < mesh.num_vertices * 3; ++i)
	{
		*(f32 *)writer = *ptr++;
		writer += sizeof(f32);
	}
	ptr = mesh.tangents;
	for (u32 i = 0; i < mesh.num_vertices * 3; ++i)
	{
		*(f32 *)writer = *ptr++;
		writer += sizeof(f32);
	}
	ptr = mesh.uvs;
	for (u32 i = 0; i < mesh.num_vertices * 2; ++i)
	{
		*(f32 *)writer = *ptr++;
		writer += sizeof(f32);
	}
	u32 *ptr2 = mesh.indices;
	for (u32 i = 0; i < mesh.num_indices; ++i)
	{
		*(u32 *)writer = *ptr2++;
		writer += sizeof(u32);
	}

	ast_free(mesh.positions);
	ast_free(mesh.normals);
	ast_free(mesh.tangents);
	ast_free(mesh.uvs);
	ast_free(mesh.indices);

	return result;

#if 0
	write(pak_fd, &length, sizeof(u32));
	write(pak_fd, &spec, sizeof(asset_specifier));
	write(pak_fd, &mesh.num_vertices, sizeof(u32));
	write(pak_fd, &mesh.num_indices, sizeof(u32));
	write(pak_fd, mesh.positions, mesh.num_vertices * 3 * sizeof(f32));
	write(pak_fd, mesh.normals, mesh.num_vertices * 3 * sizeof(f32));
	write(pak_fd, mesh.tangents, mesh.num_vertices * 3 * sizeof(f32));
	write(pak_fd, mesh.uvs, mesh.num_vertices * 2 * sizeof(f32));
	write(pak_fd, mesh.indices, mesh.num_indices * sizeof(u32));

	ast_free(mesh.positions);
	ast_free(mesh.normals);
	ast_free(mesh.tangents);
	ast_free(mesh.uvs);
	ast_free(mesh.indices);
#endif
}

data_write add_shader_asset(
		const char *vertex_filename,
		const char *fragment_filename,
		asset_specifier spec
	)
{
	file_contents vs_fc = ast_read_raw_file(vertex_filename);
	file_contents fs_fc = ast_read_raw_file(fragment_filename);
	ASSERT(vs_fc.data != 0 && vs_fc.size != 0);
	ASSERT(fs_fc.data != 0 && fs_fc.size != 0);

	game_asset_type type = ASSETTYPE_SHADER;
	u32 length = vs_fc.size + fs_fc.size + sizeof(u64) + sizeof(asset_specifier) + 2;
	
	data_write result;
	result.length = length + sizeof(game_asset_type) + sizeof(u32);
	result.ptr = ast_malloc(result.length);

	u8 *writer = result.ptr;
	*(game_asset_type *)writer = type;
	writer += sizeof(game_asset_type);
	*(u32 *)writer = length;
	writer += sizeof(u32);
	*(asset_specifier *)writer = spec;
	writer += sizeof(asset_specifier);
	*(u64 *)writer = vs_fc.size + 1;
	writer += sizeof(u64);
	u8 *ptr = vs_fc.data;
	for (u64 i = 0; i < vs_fc.size; ++i)
	{
		*writer++ = *ptr++;
	}
	*writer++ = 0;
	ptr = fs_fc.data;
	for (u64 i = 0; i < fs_fc.size; ++i)
	{
		*writer++ = *ptr++;
	}
	*writer++ = 0;

	ast_free(vs_fc.data);
	ast_free(fs_fc.data);

	return result;

#if 0
	write(pak_fd, &(game_asset_type) { ASSETTYPE_SHADER }, sizeof(game_asset_type));

	write(pak_fd, &length, sizeof(u32));
	write(pak_fd, &spec, sizeof(asset_specifier));
	write(pak_fd, &(u64){vs_fc.size + 1}, sizeof(u64));
	write(pak_fd, vs_fc.data, vs_fc.size);
	write(pak_fd, &(u8){ 0 }, 1);
	write(pak_fd, fs_fc.data, fs_fc.size);
	write(pak_fd, &(u8){ 0 }, 1);
	ast_free(vs_fc.data);
	ast_free(fs_fc.data);
#endif
}

b32 do_thread_work(work_queue *queue)
{
	pthread_mutex_lock(&queue_task_mutex);
	asset_work *work = 0;
	if (queue->current_free_task < queue->num_tasks)
	{
		work = queue->tasks + queue->current_free_task++;
	}
	pthread_mutex_unlock(&queue_task_mutex);
	
	if (work == 0)
	{
		return false;
	}
	
	switch (work->kind)
	{
	case ASSET_WORK_TEXTURE:
	{
		data_write tex_data = add_texture_asset(work->asset_filename, work->spec);
		write_data(queue->data, tex_data);
		ast_free(tex_data.ptr);
	} break;
	case ASSET_WORK_MESH:
	{
		data_write mesh_data = add_static_mesh_asset(work->asset_filename, work->spec);
		write_data(queue->data, mesh_data);
		ast_free(mesh_data.ptr);
	} break;
	case ASSET_WORK_SHADER:
	{
		data_write shader_data = add_shader_asset(
			work->shader.vs_filename,
			work->shader.fs_filename,
			work->spec
			);
		write_data(queue->data, shader_data);
		ast_free(shader_data.ptr);
	} break;
	}

	return true;
}

void *thread_start(void* data)
{
	work_queue *queue = data;
	b32 keep_running = false;
	do
	{
		keep_running = do_thread_work(queue);
	} while (keep_running);

	pthread_exit(0);
	return 0;
}

#define NUM_THREADS 7
void run_asset_gen(work_queue *queue)
{
	pthread_t thread_ids[NUM_THREADS];
	for (u32 i = 0; i < NUM_THREADS; ++i)
	{
		int err = pthread_create(&thread_ids[i], NULL, thread_start, queue);
		if (err != 0)
		{
			printf("failed to launch thread no. %u\n", i);
		}
	}

	b32 tasks_incomplete = true;
	u32 prev_task = 0;
	while (tasks_incomplete)
	{
		pthread_mutex_lock(&queue_task_mutex);
		tasks_incomplete = queue->current_free_task < queue->num_tasks;
		u32 current_task = queue->current_free_task;
		pthread_mutex_unlock(&queue_task_mutex);
		if (current_task == 0 || current_task != prev_task)
		{
			prev_task = current_task;
			printf("\rcurrent_task: %u - num_tasks: %u", current_task, queue->num_tasks);
		}
	}
	printf("\n");
	
	for (u32 i = 0; i < NUM_THREADS; ++i)
	{
		pthread_join(thread_ids[i], NULL);
	}
}

s32 main(void)
{
	ast_timer timer = ast_create_timer();

	pak_data data;
	size_t data_start_length = MEGABYTES(1);
	data.data = ast_malloc(data_start_length);
	data.write_offset = 0;
	data.capacity = data_start_length;

	work_queue asset_queue;
	asset_queue.num_tasks = 0;
	asset_queue.current_free_task = 0;
	asset_queue.data = &data;
	
	// #todo apl_png.h has a memory leak with it's chunks. Not all of them are being free'd,
	// though a lot of them are.
	add_asset_to_queue(&asset_queue, "textures/white.png", ASSETTYPE_TEXTURE, ASSET_TEXTURE_WHITE);
	add_asset_to_queue(&asset_queue, "textures/normal_map.png", ASSETTYPE_TEXTURE, ASSET_TEXTURE_NORMAL_PLAIN);
	add_asset_to_queue(&asset_queue, "textures/albedo_metallic_map.png", ASSETTYPE_TEXTURE, ASSET_TEXTURE_DIFFUSE_CUBE);
	add_asset_to_queue(&asset_queue, "textures/normal_roughness_map.png", ASSETTYPE_TEXTURE, ASSET_TEXTURE_NORMAL_CUBE);
	add_asset_to_queue(&asset_queue, "textures/plane_normal_map.png", ASSETTYPE_TEXTURE, ASSET_TEXTURE_NORMAL_PLANE);
	add_asset_to_queue(&asset_queue, "textures/sphere_normal_map.png", ASSETTYPE_TEXTURE, ASSET_TEXTURE_NORMAL_SPHERE);
	add_asset_to_queue(&asset_queue, "textures/grid_2048.png", ASSETTYPE_TEXTURE, ASSET_TEXTURE_DIFFUSE_CHARACTER);
	add_asset_to_queue(&asset_queue, "textures/character_normal.png", ASSETTYPE_TEXTURE, ASSET_TEXTURE_NORMAL_CHARACTER);
	add_asset_to_queue(&asset_queue, "textures/skybox.png", ASSETTYPE_TEXTURE, ASSET_TEXTURE_SKYBOX);

	add_asset_to_queue(&asset_queue, "meshes/cube.dae", ASSETTYPE_MESH, ASSET_MESH_CUBE);
	add_asset_to_queue(&asset_queue, "meshes/plane.dae", ASSETTYPE_MESH, ASSET_MESH_PLANE);
	add_asset_to_queue(&asset_queue, "meshes/sphere.dae", ASSETTYPE_MESH, ASSET_MESH_SPHERE);
	add_asset_to_queue(&asset_queue, "meshes/mannequin.dae", ASSETTYPE_MESH, ASSET_MESH_CHARACTER);
	add_asset_to_queue(&asset_queue, "meshes/sphere_hp2.dae", ASSETTYPE_MESH, ASSET_MESH_SPHERE_HP);

	add_shader_to_queue(&asset_queue,
						"shaders/skybox.vs",
						"shaders/skybox.fs",
						ASSET_SKYBOX_SHADER);
	add_shader_to_queue(
		&asset_queue,
		"shaders/gbuffer.vs",
		"shaders/gbuffer.fs",
		ASSET_GBUFFER_SHADER
		);
	add_shader_to_queue(
		&asset_queue,
		"shaders/deferred_shading.vs",
		"shaders/deferred_pbr.fs",
		ASSET_DEFERRED_SHADER
		);

	run_asset_gen(&asset_queue);

	// #note write pak data to file
	s32 pak_fd;
	char *filename = "asset_pak.blk";
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH;
	pak_fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, mode);
	if (pak_fd == -1)
	{
		printf("failed to open '%s' for pak creation\n", filename);
		return 1;
	}
	ssize_t bytes_written = write(pak_fd, "BLEAK_ASSET_PACK", 16);
	if (bytes_written != 16)
	{
		printf("failed to write magic number to pak file, exiting\n");
		close(pak_fd);
		return 1;
	}
	bytes_written = write(pak_fd, data.data, data.write_offset);
	if ((u64)bytes_written != data.write_offset)
	{
		printf("incorrect number of bytes written to pak file, likely corruption\n");
	}
	close(pak_fd);
	
	ast_update_timer(&timer);
	printf("pak build time: %f s\n", (f32)timer.nanoseconds / 1000000000.0f);

	/* #note
	   log of times pre threading
	   6.64 s
	   6.62 s
	   6.70 s
	   6.66 s
	   avg: 6.66 s

	   log of times after restructure, no threading
	   6.82 s
	   6.87 s
	   6.85 s
	   6.83 s
	   avg: 6.84 s

	   with threads it takes ~7 seconds, currently the time taken is
	   so overwhelmed by the character mesh, which is the only
	   significant task, that data collection is kind of meaningless,
	   and optimization of the mesh code would be more fruitful.
	 */

	return 0;
}
