@echo off

ctags .\*.h .\*.c ..\..\ast\*.h ..\..\apl\*.h

rem 0 for debug, 1 for release
set config=0

set exe_name=win32_bleak
set dll_name=bleak

set exe_inc=-I..\..\ast\ -I..\..\apl\
set dll_inc=-I..\..\ast\ -I..\..\apl\

set exe_libs=opengl32.lib ole32.lib gdi32.lib user32.lib kernel32.lib msvcrt.lib ..\..\apl\lib\apl.lib
set dll_libs=msvcrt.lib
rem kernel32.lib msvcrt.lib comdlg32.lib winspool.lib shell32.lib

set warning_ignore=-wd4100 -wd4189 -wd4201 -wd4477 -wd4099 -wd4200 -wd4204
set warning=-WX -W4 %warning_ignore%

set debug_vars=
set opt=

if "%config%"=="0" (
	echo debug
	set debug_vars="/DDEBUG_PRINT=0"
	set opt="-Od"
) else (
	echo release
	set debug_vars=""
	set opt="-O3"
)

set opt=%opt% -Oi -fp:fast
set misc=-nologo -Z7 -FC -Gm- -MTd -GR- -EHsc

set nodeflibs=-nodefaultlib:msvcrt.lib -nodefaultlib:libcmt.lib -nodefaultlib:libcmtd.lib
set export_fns=-export:host_frame
set common_cflags=%debug_vars% %warning% %opt% %misc%
set common_lflags=-incremental:no -opt:ref %nodeflibs%

set exe_lflags=%common_lflags% %exe_libs% -out:%exe_name%.exe
set dll_lflags=%common_lflags% %dll_libs% -out:%dll_name%.dll %export_fns%
rem -ignore:4099
if not exist ..\build mkdir ..\build
if not exist ..\data mkdir ..\data

pushd ..\build

cl %common_cflags% %exe_inc% -Fm%exe_name%.map ..\src\engine_sys.c /link %exe_lflags%

cl %common_cflags% %dll_inc% -Fm%dll_name%.map ..\src\host.c -LD /link %dll_lflags%

move %exe_name%.exe ..\data\%exe_name%.exe
move %dll_name%.dll ..\data\%dll_name%.dll

popd
